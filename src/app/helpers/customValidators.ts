import { FormControl } from '@angular/forms';

export class CustomValidators {
  
  constructor() { }

  static required(control: FormControl) {
    if (control.value == null) {
      let isValid
      return isValid ? null : { 'emptyValue': true }
    } else {
      let isValid = !((control.value + "").trim().length === 0);
      return isValid ? null : { 'emptyValue': true }
    }
  }

 

}
