
import { InformacionAmostrarTiempoReal } from './informacion-a-mostrar-tiempo-real.interface';
import { FiltrosTiempoReal } from './filtros-tiempo-real.interface';

export interface SeleccionPanelTiempoReal {
    infoSeleccionada: InformacionAmostrarTiempoReal,
    filtrosSeleccionados: FiltrosTiempoReal
}

