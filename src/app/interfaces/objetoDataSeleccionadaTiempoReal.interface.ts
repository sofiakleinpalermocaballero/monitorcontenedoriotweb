export interface ObjetoSeleccionTiempoReal {
    filtroNivelLlenado : string,
    filtroAlertas:string[]
    mostrarAlertas: boolean,
    mostrarPorcentajeLlenado: boolean
}