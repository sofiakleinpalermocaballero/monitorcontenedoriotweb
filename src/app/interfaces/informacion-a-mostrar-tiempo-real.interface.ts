export interface InformacionAmostrarTiempoReal {
    mostrarAlertas: boolean,
    mostrarPorcentajeLlenado: boolean
}