import { BrowserModule } from '@angular/platform-browser';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import {AngularFireAuthModule } from '@angular/fire/auth';
import {firebaseConfig} from '../environments/firebase.config';
import {HttpClientModule} from '@angular/common/http';
import { NgbModule,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
// import { MDBBootstrapModule } from 'angular-bootstrap-md';
// import {MatDialogModule} from '@angular/material';

// import { LeafletModule } from '@asymmetrik/angular2-leaflet';
// import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// Componentes
import { AppComponent } from './app.component';
import { EncabezadoComponent } from './componentes/encabezado/encabezado.component';
import { PanelNotificacionesComponent } from './componentes/alertas/panel-notificaciones/panel-notificaciones.component';
import { PrincipalComponent } from './componentes/principal/principal.component';
import { MapaComponent } from './componentes/tiempoReal/mapa-tiempo-real/mapa-tiempo-real.component';
import { VisualizacionReportesComponent } from './componentes/reportes/visualizacion-reportes/visualizacion-reportes.component';
import { VisualizacionTiempoRealComponent } from './componentes/tiempoReal/visualizacion-tiempo-real/visualizacion-tiempo-real.component';
import { VisReporteAlertasComponent } from './componentes/reportes/contenedoresParaCadaReporte/vis-reporte-alertas/vis-reporte-alertas.component';
import { VisReporteLlenadoPromRecoleccionComponent } from './componentes/reportes/contenedoresParaCadaReporte/vis-reporte-llenado-prom-recoleccion/vis-reporte-llenado-prom-recoleccion.component';
import { VisReporteTiempoPromRecoleccionComponent } from './componentes/reportes/contenedoresParaCadaReporte/vis-reporte-tiempo-prom-recoleccion/vis-reporte-tiempo-prom-recoleccion.component';
import { VisReporteFuncionamientoComponent } from './componentes/reportes/contenedoresParaCadaReporte/vis-reporte-funcionamiento/vis-reporte-funcionamiento.component';
import { AlertaTarjetaComponent } from './componentes/alertas/alerta-tarjeta/alerta-tarjeta.component';
// import { MapaReporteAlertasComponent } from './componentes/mapa-reporte-alertas/mapa-reporte-alertas.component';
import { MapaFlotanteAlertaComponent } from './componentes/alertas/mapa-flotante-alerta/mapa-flotante-alerta.component';
import { MapaReportesComponent } from './componentes/reportes/mapa-reportes/mapa-reportes.component';
import { VisReporteVaciadosTurnoComponent } from './componentes/reportes/contenedoresParaCadaReporte/vis-reporte-vaciados-turno/vis-reporte-vaciados-turno.component';
import {DialogComponent} from './helpers/dialog/dialog.component';
import { GrillaReporteAlertasComponent } from './componentes/reportes/grilla-reporte-alertas/grilla-reporte-alertas.component';
import { LoginComponent } from './componentes/login/login.component'
import { ConfiguracionesComponent } from './componentes/configuraciones/configuraciones.component';
import { InicioComponent } from './componentes/inicio/inicio.component';



import {APP_ROUTING} from './app.routes';


// Servicios
import { ApiService } from './servicios/api-service';
import {AuthService } from './servicios/auth.service';
import {AuthGuardService, AuthGuardLogin,AuthGuardLogueado} from './servicios/auth-guard.service';
import { UsuariosComponent } from './componentes/gestionUsuarios/usuarios/usuarios.component';
import { AltaEdicionUsuarioComponent } from './componentes/gestionUsuarios/alta-edicion-usuario/alta-edicion-usuario.component';
import { EdicionPasswordComponent } from './componentes/gestionUsuarios/edicion-password/edicion-password.component';
import { ImportacionContenedoresComponent } from './componentes/contenedores/importacion-contenedores/importacion-contenedores.component';
import { NgDropFilesDirective } from './directives/ng-drop-files.directive';
import { CircuitosComponent } from './componentes/circuitos/circuitos.component';
import { VisReporteHistoricoRecoleccionesComponent } from './componentes/reportes/contenedoresParaCadaReporte/vis-reporte-historico-recolecciones/vis-reporte-historico-recolecciones.component';



@NgModule({
  declarations: [
    AppComponent,
    EncabezadoComponent,
    PanelNotificacionesComponent,
    PrincipalComponent,
    MapaComponent,
    VisualizacionReportesComponent,
    VisualizacionTiempoRealComponent,
    VisReporteAlertasComponent,
    VisReporteLlenadoPromRecoleccionComponent,
    VisReporteTiempoPromRecoleccionComponent,
    VisReporteFuncionamientoComponent,
    AlertaTarjetaComponent,
    // MapaReporteAlertasComponent,
    MapaFlotanteAlertaComponent,
    MapaReportesComponent,
    VisReporteVaciadosTurnoComponent,
    DialogComponent,
    GrillaReporteAlertasComponent,
    LoginComponent,
    ConfiguracionesComponent,
    InicioComponent,
    UsuariosComponent,
    AltaEdicionUsuarioComponent,
    EdicionPasswordComponent,
    ImportacionContenedoresComponent,
    NgDropFilesDirective,
    CircuitosComponent,
    VisReporteHistoricoRecoleccionesComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    LeafletModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
    NgbModule.forRoot(),
    NgxDatatableModule,
    APP_ROUTING,
    ReactiveFormsModule,
   // MDBBootstrapModule.forRoot()
    // MatDialogModule,
    // BrowserAnimationsModule
  ],
  entryComponents: [MapaFlotanteAlertaComponent, DialogComponent, AltaEdicionUsuarioComponent, EdicionPasswordComponent],
  providers: [NgbActiveModal, ApiService, AuthService, AuthGuardService, AuthGuardLogin, AuthGuardLogueado],
  bootstrap: [AppComponent]})

export class AppModule { }
