import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service'
import { Router } from '@angular/router';
import { ApiService } from "../../servicios/api-service";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  usuario = {
    mail: '',
    pass: ''
  };

  error: string = "";

  constructor(private router: Router, public authService: AuthService, public apiService:ApiService ) { }

  ngOnInit() {
  }

  // ingresar(){
  //this.authService.login();
  //  }

  ingresar() {
    //debugger;
    this.error = "";
    localStorage.setItem('mailUsuarioLogueado', this.usuario.mail);
    this.authService.inicioSesionRegular(this.usuario.mail, this.usuario.pass)
      .then(res => {
        //debugger;
        this.router.navigate(['inicio'])
        //return true;
      })
      .catch(error => {
       // debugger;
        if (error.code == "auth/wrong-password") {
          this.error = "Contraseña inválida."
        } else {
          if (error.code == "auth/invalid-email") {
            this.error = "El correo electrónico especificado no se encuentra registrado."
          }
        }
        
        console.log(error)
        //return false;
      });
    /*if(this.authService.inicioSesionRegular(this.usuario.mail, this.usuario.pass)){
      
      this.router.navigate(['inicio']);
    }else{
      this.error="Error iniciando sesión"
    }
    */
  }

}
