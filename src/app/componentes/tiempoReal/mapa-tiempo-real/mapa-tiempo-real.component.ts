import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
//import { SeleccionPanelTiempoReal } from '../../interfaces/seleccion-panel-tiempo-real.interface';
import { ObjetoSeleccionTiempoReal } from '../../../interfaces/objetoDataSeleccionadaTiempoReal.interface';
import { LimiteGeografico } from '../../../interfaces/limite-geografico.interface';
import { HttpClient } from '@angular/common/http';

import * as L from 'leaflet';

@Component({
  selector: 'app-mapa-tiempo-real',
  templateUrl: './mapa-tiempo-real.component.html',
  styleUrls: ['./mapa-tiempo-real.component.css']
})
export class MapaComponent implements OnInit, OnChanges {

  //@Input() seleccionPanelTiempoReal:SeleccionPanelTiempoReal;

  @Input() tipoSeleccion: string;
  @Input() datosVentanaTiempoReal: ObjetoSeleccionTiempoReal;
  @Input() filtroMunicipio: any;//LimiteGeografico
  @Input() filtroCircuito: any;//LimiteGeografico

  /*@Input() seleccionPanelTiempoReal = {
    informacion:{ 
      mostrarAlertas: false,
      mostrarPorcentajeLlenado:false
    },
    filtros: {
      nivelLlenado : '',
      alertas:[]
    }
  };
  */


  municipios: any; //= [{ "MunicipioId": 1, "Codigo": "A", "Limites": [] }, { "MunicipioId": 2, "Codigo": "B", "Limites": [[-34.92929, -56.1611671], [-34.88903, -56.16563], [-34.88776, -56.1646843], [-34.8895226, -56.18211], [-34.8904381, -56.18168], [-34.8914948, -56.1862259], [-34.8904381, -56.18597], [-34.8899422, -56.1879425], [-34.89093, -56.1882858], [-34.8912125, -56.2031364], [-34.89635, -56.2003021], [-34.9058533, -56.2233047], [-34.91275, -56.2169533], [-34.9114838, -56.20262], [-34.9 80456, -56.1854553], [-34.91613, -56.1717224], [-34.91937, -56.1737], [-34.9239426, -56.173008], [-34.92929, -56.1611671]] }, { "MunicipioId": 3, "Codigo": "C", "Limites": [[-34.8790321, -56.1555862], [-34.8766365, -56.15095], [-34.8460732, -56.19009], [-34.87213, -56.2266541], [-34.8829727, -56.1998749], [-34.8914223, -56.2031364], [-34.8908577, -56.1882], [-34.8897324, -56.18786], [-34.8902969, -56.18597], [-34.8914223, -56.186142], [-34.8901558, -56.1820221], [-34.88959, -56.18185], [-34.88762, -56.1646843], [-34.8852272, -56.16314], [-34.8790321, -56.1555862]] }, { "MunicipioId": 4, "Codigo": "CH", "Limites": [[-34.9373131, -56.16022], [-34.88889, -56.1655426], [-34.88579, -56.1634827], [-34.8790321, -56.1554146], [-34.888607, -56.1473465], [-34.886776, -56.14237], [-34.8863525, -56.1377335], [-34.89959, -56.1222839], [-34.9040947, -56.1222839], [-34.91226, -56.131897], [-34.9111328, -56.14065], [-34.9132423, -56.1452866], [-34.9185944, -56.1473465], [-34.919014, -56.14374], [-34.9284439, -56.1578178], [-34.9373131, -56.16022]] }, { "MunicipioId": 5, "Codigo": "D", "Limites": [] }, { "MunicipioId": 6, "Codigo": "E", "Limites": [] }, { "MunicipioId": 7, "Codigo": "F", "Limites": [] }, { "MunicipioId": 8, "Codigo": "G", "Limites": [] }];
  circuitos: any; //= [{ "MunicipioId": 4, "Circuitos": [{ "CircuitoId": 1, "Codigo": "CH_1", "Municipio": "CH", "Limites": [[-34.9152832, -56.14876], [-34.91166, -56.1455421], [-34.9075775, -56.14949], [-34.90677, -56.1495361], [-34.9068031, -56.1504364], [-34.9087753, -56.1548576], [-34.90916, -56.1534424], [-34.9106064, -56.152195], [-34.91215, -56.15357], [-34.91409, -56.1534424], [-34.9152832, -56.14876]] }, { "CircuitoId": 2, "Codigo": "CH_2", "Municipio": "CH", "Limites": [[-34.91409, -56.1534424], [-34.9192619, -56.15619], [-34.92183, -56.15065], [-34.92081, -56.1493645], [-34.91838, -56.14855], [-34.9152145, -56.14889], [-34.91409, -56.1534424]] }, { "CircuitoId": 3, "Codigo": "CH_3", "Municipio": "CH", "Limites": [[-34.91099, -56.16314], [-34.9119759, -56.16022], [-34.9087029, -56.15481], [-34.9091263, -56.1534424], [-34.91064, -56.1522827], [-34.91219, -56.153656], [-34.91409, -56.1534843], [-34.91891, -56.15606], [-34.9165878, -56.1584625], [-34.91838, -56.16121], [-34.9171867, -56.1624527], [-34.91099, -56.16314]] }, { "CircuitoId": 4, "Codigo": "CH_4", "Municipio": "CH", "Limites": [[-34.91722, -56.1624527], [-34.9183464, -56.16121], [-34.9165535, -56.1584625], [-34.9189453, -56.15606], [-34.9192619, -56.15619], [-34.92183, -56.1506081], [-34.92929, -56.16112], [-34.91722, -56.1624527]] }] }, { "MunicipioId": 3, "Circuitos": [{ "CircuitoId": 6, "Codigo": "C_1", "Municipio": "C", "Limites": [[-34.87086, -56.1823235], [-34.86287, -56.17919], [-34.863678, -56.1768723], [-34.86579, -56.17348], [-34.862236, -56.1700478], [-34.86794, -56.1625824], [-34.8698769, -56.1677742], [-34.87086, -56.1823235]] }, { "CircuitoId": 7, "Codigo": "C_2", "Municipio": "C", "Limites": [[-34.88505, -56.19614], [-34.88498, -56.1946831], [-34.88403, -56.19481], [-34.8839226, -56.19378], [-34.88283, -56.19138], [-34.8837128, -56.1907768], [-34.8820572, -56.1873], [-34.883255, -56.1867], [-34.8810539, -56.1863136], [-34.8827972, -56.1817551], [-34.88809, -56.1847458], [-34.889267, -56.1816254], [-34.8906479, -56.1813774], [-34.8918343, -56.1863365], [-34.8926544, -56.1866455], [-34.8919334, -56.18874], [-34.8910446, -56.18831], [-34.8915977, -56.1954956], [-34.88505, -56.19614]] }] }, { "MunicipioId": 2, "Circuitos": [{ "CircuitoId": 5, "Codigo": "B_1", "Municipio": "B", "Limites": [[-34.895752, -56.18168], [-34.8923721, -56.1753273], [-34.89167, -56.1652], [-34.88769, -56.1656723], [-34.88924, -56.18168], [-34.8906136, -56.1815071], [-34.8912125, -56.1842537], [-34.895752, -56.18168]] }] }];

  //data: any = [{ "fire": { "metadata": {}, "type": "Float", "value": 1 }, "heartbeat": { "metadata": {}, "type": "Integer", "value": 1 }, "id": "A", "location": { "metadata": {}, "type": "geo:json", "value": { "coordinates": [-34.91063982, -56.150736809], "type": "Point" } }, "overturn": { "metadata": {}, "type": "Integer", "value": 0 }, "trash_level": { "metadata": {}, "type": "Integer", "value": 54 }, "type": "Container" }, { "fire": { "metadata": {}, "type": "Float", "value": 0 }, "heartbeat": { "metadata": {}, "type": "Integer", "value": 0 }, "id": "B", "location": { "metadata": {}, "type": "geo:json", "value": { "coordinates": [-34.910569435, -56.150608063], "type": "Point" } }, "overturn": { "metadata": {}, "type": "Integer", "value": 0 }, "trash_level": { "metadata": {}, "type": "Integer", "value": 30 }, "type": "Container" }, { "fire": { "metadata": {}, "type": "Float", "value": 0 }, "heartbeat": { "metadata": {}, "type": "Integer", "value": 0 }, "id": "C", "location": { "metadata": {}, "type": "geo:json", "value": { "coordinates": [-34.911396457, -56.151809692], "type": "Point" } }, "overturn": { "metadata": {}, "type": "Integer", "value": 0 }, "trash_level": { "metadata": {}, "type": "Integer", "value": 15 }, "type": "Container" }, { "fire": { "metadata": {}, "type": "Float", "value": 0 }, "heartbeat": { "metadata": {}, "type": "Integer", "value": 0 }, "id": "D", "location": { "metadata": {}, "type": "geo:json", "value": { "coordinates": [-34.910076738, -56.151251793], "type": "Point" } }, "overturn": { "metadata": {}, "type": "Integer", "value": 0 }, "trash_level": { "metadata": {}, "type": "Integer", "value": 75 }, "type": "Container" }];
  items: Observable<any[]>;
  contenedores: any;

  //usados por leaflet:
  mymap: any;
  groupMarkersAlertas: any;
  groupMarkersContenedores: any;
  //icons:
  greenIcon: any;
  yellowIcon: any;
  redIcon: any;
  blackIcon:any;
  alertIcon: any;

  constructor(afs: AngularFirestore, private http: HttpClient) {
    this.items = afs.collection('contenedores').valueChanges();
  }

  async ngOnInit() {
    var muni=await this.getMunicipios()
    
    this.municipios = muni["Datos"];
    var circs= await this.getCircuitos();
    this.circuitos = circs["Datos"];
    this.initializeMap();
  }

  getMunicipios() {
    return this.http.get('http://localhost:49570/api/adminContenedor/municipiosConLim').toPromise();
    /*this.http.get('http://localhost:49570/api/adminContenedor/municipios').subscribe(
       resp => {
        
        this.municipios = resp;
        console.log(resp);
      },
      err => {
        console.log(err);
        console.log("Error occured.")
      }
    )*/
  }

  getCircuitos() {
    return this.http.get('http://localhost:49570/api/adminContenedor/circuitosConLim').toPromise();
    /*this.http.get('http://localhost:49570/api/adminContenedor/municipios').subscribe(
       resp => {
        
        this.municipios = resp;
        console.log(resp);
      },
      err => {
        console.log(err);
        console.log("Error occured.")
      }
    )*/
  }

  ngOnChanges(cambio: SimpleChanges) {
    
    console.log("cambios:");
    console.log(cambio);

    if (cambio.datosVentanaTiempoReal && !cambio.datosVentanaTiempoReal.firstChange) {
      this.visualizarContainers();
      console.log("datos ventana");
    } else {
      if (cambio.filtroMunicipio && !cambio.filtroMunicipio.firstChange) {
        console.log("filtro municipio");
        this.fitBoundsMunicipio()
      } else {
        if (cambio.filtroCircuito && !cambio.filtroCircuito.firstChange) {
          if (this.filtroCircuito == "") {
            this.fitBoundsMunicipio();
          } else {
            var circuito = this.circuitos.filter(item => item.CircuitoId == this.filtroCircuito.CircuitoId)[0];
            this.mymap.fitBounds(circuito.Limites, { padding: [0, 0] });
          }
        }

      }
      //this.tipoSeleccion;//depende del tipo de seleccion, los items del mapa se tienen q esconder/mostrar
    }
  }

  fitBoundsMunicipio() {
    if (this.filtroMunicipio == "") {
      this.centrarMapa();
    } else {
      var municipio = this.municipios.filter(item => item.MunicipioId == this.filtroMunicipio.MunicipioId)[0];
      this.mymap.fitBounds(municipio.Limites, { padding: [0, 0] });
    }
  }

  centrarMapa() {
    this.mymap.setView([-34.89437925718939, -56.16554260253906], 13);
  }

  initializeMap() {
    this.mymap = L.map('mapid');

    //L.tileLayer('https://korona.geog.uni-heidelberg.de/tiles/roads/x={x}&y={y}&z={z}', {
    //	attribution: 'Imagery from <a href="http://giscience.uni-hd.de/">GIScience Research Group @ University of Heidelberg</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    //}).addTo(mymap);

    L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png', {
      attribution: '<a href="https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">Wikimedia</a>'
    }).addTo(this.mymap);

    this.greenIcon = L.icon({
      iconUrl: 'assets/img/icon_verde.svg',
      iconSize: [48, 55],
      iconAnchor: [24, 55],
      popupAnchor: [0, -55]
    });

    this.yellowIcon = L.icon({
      iconUrl: 'assets/img/icon_amarillo.svg',
      iconSize: [48, 55],
      iconAnchor: [24, 55],
      popupAnchor: [0, -55]
    });

    this.redIcon = L.icon({
      iconUrl: 'assets/img/icon_rojo.svg',
      iconSize: [48, 55],
      iconAnchor: [24, 55],
      popupAnchor: [0, -55]
    });
    this.blackIcon = L.icon({
      iconUrl: 'assets/img/icon_black.svg',
      iconSize: [48, 55],
      iconAnchor: [24, 55],
      popupAnchor: [0, -55]
    });

    this.alertIcon = L.icon({
      iconUrl: 'assets/img/alerta.svg',
      iconSize: [38, 45],
      iconAnchor: [20, 95],
      popupAnchor: [0, -55]
    });

    this.municipios.forEach((municipio: any) => {
      L.polyline(municipio.Limites, { color: 'red' }).addTo(this.mymap);
    });

    this.circuitos.forEach((cirtuito: any) => {
      L.polyline(cirtuito.Limites).addTo(this.mymap);
    });
    this.items.subscribe(res => {
      this.contenedores = res;
      this.visualizarContainers();
    });
    this.centrarMapa();



    //this.data.forEach((item: any) => {
    //this.documents.push(item.DocumentName);
    //});

    /*
        this.data.forEach((container: any) => {
          console.log(container.trash_level);
          
          var geoLocation = container.location;
          var coordinates = [geoLocation.latitude, geoLocation.longitude];
          var marker = L.marker(coordinates, {
            icon: greenIcon
          });
          if (container.trash_level > 40 && container.trash_level < 75) {
            marker = L.marker(coordinates, {
              icon: yellowIcon
            });
          }
          if (container.trash_level >= 75) {
            marker = L.marker(coordinates, {
              icon: redIcon
            });
          }
          marker.bindPopup("<b>Hello world!</b><br />I am a popup.");
          markers.push(marker);
        });
        */


    /*mymap.fitBounds(featureGroup.getBounds(), {
      padding: [20, 20]
    });*/


  }



  visualizarContainers() {
    var markersAlertas = [];
    var markersContenedores = [];

    this.contenedores.forEach((container: any) => {
      var mostrarAlerta = false;
      var mostrarContenedor = true;
      console.log(container.nivel_llenado);
      var geoLocation = container.ubicacion;

      var coordinates = L.latLng([geoLocation.latitude, geoLocation.longitude]);
      var icon = this.greenIcon;

      var nivelLlenadoContenedor = 'bajo';
      if (container.nivel_llenado > 40 && container.nivel_llenado < 75) {
        icon = this.yellowIcon;
        nivelLlenadoContenedor = 'medio';
      }
      if (container.nivel_llenado >= 75 && container.nivel_llenado <= 99) {
        icon = this.redIcon;
        nivelLlenadoContenedor = 'alto';
      }
      if (container.nivel_llenado >=100) {
        icon = this.blackIcon;
        nivelLlenadoContenedor = ''; //ACA Q PONEMOS????
      }

      var contenido = "Municipio: " + container.municipio + "<br />" +
        "Circuito: " + container.circuito + "<br />";
      

      if (this.groupMarkersContenedores != undefined) {
        this.mymap.removeLayer(this.groupMarkersContenedores);
      }
      if (this.groupMarkersAlertas != undefined) {
        this.mymap.removeLayer(this.groupMarkersAlertas);
      }
      if (this.datosVentanaTiempoReal != undefined && this.datosVentanaTiempoReal.mostrarAlertas) {
        if (this.datosVentanaTiempoReal.filtroAlertas.length > 0 && this.datosVentanaTiempoReal.filtroAlertas.length < 5) {
          mostrarContenedor = false;
          if (this.datosVentanaTiempoReal.filtroAlertas.filter(item => item == 'Sin alertas').length > 0) {
            if (this.datosVentanaTiempoReal.filtroAlertas.length > 1) {
              //hay que mostrar los que no tienen ninguna alerta y los que tienen alguna de las seleccionadas
              this.datosVentanaTiempoReal.filtroAlertas.forEach((tipoAlerta: any) => {
                if (container.fuego > 0 && tipoAlerta == 'Incendio') {
                  contenido = contenido + "Alerta de incendio <br />";
                  mostrarAlerta = true;
                }
                if (container.volcado > 0 && tipoAlerta == 'Volcado') {
                  contenido = contenido + "Alerta de volcado <br />";
                  mostrarAlerta = true;
                }
                if (container.seres_vivos > 0 && tipoAlerta == 'Seres vivos') {
                  contenido = contenido + "Posible ser vivo detectado <br />";
                  mostrarAlerta = true;
                }
                // Si hay mal func no deberia haber otro tipo de alerta
                /*if (container.funcionamiento > 0 && tipoAlerta=='Funcionamiento') {
                  contenido = contenido + "Mal funcionamiento del contenedor <br />";
                }*/
              });
              if (mostrarAlerta || (container.fuego == 0 && container.volcado == 0 && container.seres_vivos == 0)) {
                mostrarContenedor = true;
              }
            } else {
              //solo mostrar si no tienen ninguna alerta
              if (container.fuego == 0 && container.volcado == 0 && container.seres_vivos == 0) {
                mostrarContenedor = true;
              }
            }
          } else {
            //mostrar solo los que tienen alguna de las seleccionadas
            this.datosVentanaTiempoReal.filtroAlertas.forEach((tipoAlerta: any) => {
              if (container.fuego > 0 && tipoAlerta == 'Incendio') {
                contenido = contenido + "Alerta de incendio <br />";
                mostrarAlerta = true;
                mostrarContenedor = true;
              }
              if (container.volcado > 0 && tipoAlerta == 'Volcado') {
                contenido = contenido + "Alerta de volcado <br />";
                mostrarAlerta = true;
                mostrarContenedor = true;
              }
              if (container.seres_vivos > 0 && tipoAlerta == 'Seres vivos') {
                contenido = contenido + "Posible ser vivo detectado <br />";
                mostrarAlerta = true;
                mostrarContenedor = true;
              }
              // Si hay mal func no deberia haber otro tipo de alerta
              /*if (container.funcionamiento > 0 && tipoAlerta=='Funcionamiento') {
                contenido = contenido + "Mal funcionamiento del contenedor <br />";
              }*/
            });
          }
        } else {
          //Casos ver todas las alertas
          if (container.fuego > 0) {
            contenido = contenido + "Alerta de incendio <br />";
            mostrarAlerta = true;
          }
          if (container.volcado > 0) {
            contenido = contenido + "Alerta de volcado <br />";
            mostrarAlerta = true;
          }
          if (container.seres_vivos > 0) {
            contenido = contenido + "Posible ser vivo detectado <br />";
            mostrarAlerta = true;
          }
          // Si hay mal func no deberia haber otro tipo de alerta
          /*if (container.funcionamiento > 0 && tipoAlerta=='Funcionamiento') {
            contenido = contenido + "Mal funcionamiento del contenedor <br />";
          }*/
        }
      }

      if (this.datosVentanaTiempoReal != undefined && this.datosVentanaTiempoReal.filtroNivelLlenado != "") {
        var nivelLlenadoSeleccionado = this.datosVentanaTiempoReal.filtroNivelLlenado;
        if (nivelLlenadoSeleccionado != nivelLlenadoContenedor) {
          mostrarContenedor = false;
          mostrarAlerta = false;
        }
      }

      if (mostrarContenedor) {
        var marker = L.marker(coordinates/*.value.coordinates*/, {
          icon: icon
        });
        marker.bindPopup(contenido);
        if (this.datosVentanaTiempoReal != undefined && this.datosVentanaTiempoReal.mostrarPorcentajeLlenado) {
          //contenido = contenido + "Llenado: " + container.nivel_llenado + "<br />";
          marker.bindTooltip(container.nivel_llenado + "%",
            {
              permanent: true,
              direction: 'bottom'
            }).openTooltip();
          
        }
        markersContenedores.push(marker);
      }
      
      if (mostrarAlerta) {
        var marker = L.marker(coordinates/*.value.coordinates*/, {
          icon: this.alertIcon
        });
        markersAlertas.push(marker);
      }


    });
    this.groupMarkersAlertas = L.featureGroup(markersAlertas).addTo(this.mymap);
    this.groupMarkersContenedores = L.featureGroup(markersContenedores).addTo(this.mymap);

  }

}
