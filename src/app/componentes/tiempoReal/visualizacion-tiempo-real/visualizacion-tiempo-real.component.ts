import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SeleccionPanelTiempoReal } from '../../../interfaces/seleccion-panel-tiempo-real.interface';
import { ObjetoSeleccionTiempoReal } from '../../../interfaces/objetoDataSeleccionadaTiempoReal.interface';
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { ApiService } from "../../../servicios/api-service";
import { DialogComponent } from '../../../helpers/dialog/dialog.component';

@Component({
  selector: 'app-visualizacion-tiempo-real',
  templateUrl: './visualizacion-tiempo-real.component.html',
  styleUrls: ['./visualizacion-tiempo-real.component.css']
})
export class VisualizacionTiempoRealComponent implements OnInit {
  alertas: any;
  selectedAllAlertas: any;
  //@Output() infoPorcentajeLlenado: EventEmitter<boolean>

  //@Output() seleccionAenviar:EventEmitter<SeleccionPanelTiempoReal>;

  //itemsSeleccionadosTiempoReal:any;
  //seleccion:SeleccionPanelTiempoReal;

  //variables para ngModel:
  mostrarAlertasCheckbox: any;
  porcentajeLlenadoCheckbox: any;
  nivelLlenado: any;



  /*todasAlertas: any;
  checkTodasAlertas: boolean;
  incendioAlerta: any;
  */


  //////////////////////////////////////////////////////////////////////////////////////    
  @Output() objetoDatosSeleccionados: EventEmitter<ObjetoSeleccionTiempoReal>;
  itemsSeleccionados: any;
  datosSeleccionados: ObjetoSeleccionTiempoReal
  //////////////////////////////////////////////////////////////////////////////////////    


  constructor(public apiService: ApiService, private modalService: NgbModal) {
    this.obtenerAlertas();
    this.nivelLlenado = "";
    this.alertas = [];

    //////////////////////////////////////////////////////////////////////////////////////   
    this.objetoDatosSeleccionados = new EventEmitter();

    this.itemsSeleccionados = {
      filtroNivelLlenado: '',
      filtroAlertas: [],
      mostrarAlertas: false,
      mostrarPorcentajeLlenado: false
    };
    this.datosSeleccionados = this.itemsSeleccionados;
    //////////////////////////////////////////////////////////////////////////////////////   
    //this.infoPorcentajeLlenado = new EventEmitter();
    /*
    this.seleccionAenviar= new EventEmitter();    
    this.itemsSeleccionadosTiempoReal = {
      infoSeleccionada:{ 
        mostrarAlertas: false,
        mostrarPorcentajeLlenado:false
      },
      filtrosSeleccionados: {
        nivelLlenado : '',
        alertas:[]
      }
    };
    this.seleccion=this.itemsSeleccionadosTiempoReal;
    */
  }

  ngOnInit() {
    
  }

  obtenerAlertas() {

    var url = "api/manejadorAlertas/tiposAlertas";
    this.apiService.get(url).subscribe(
      resp => {
        
        var ok = resp["Exito"];
        var msj = resp["Mensaje"];
        if (!ok) {
          this.openDialog(msj, "Error")
        } else {
          var datos = resp["Datos"];
          if (datos == null || datos.length == 0) {
            this.openDialog(msj, "Atención");
          } else {
            


            /*      { name: 'Incendio', selected: false },
                  { name: 'Seres vivos', selected: false },
                  { name: 'Funcionamiento', selected: false },
                  { name: 'Volcado', selected: false } */

            datos.forEach((alerta: any) => {
             
              var item = { name: alerta.Descripcion, selected: false };
              this.alertas.push(item);
            })
          }
        }

      },
      err => {
        console.log(err);
        console.log("Error occured.")
      }
    )
  }


  mostrarInfoAlertasClick() {
    this.datosSeleccionados.mostrarAlertas = this.mostrarAlertasCheckbox;
    this.objetoDatosSeleccionados.emit(this.datosSeleccionados);
  }

  porcentajeLlenadoClick() {
    this.datosSeleccionados.mostrarPorcentajeLlenado = this.porcentajeLlenadoCheckbox;
    this.objetoDatosSeleccionados.emit(this.datosSeleccionados);
    //this.seleccion.infoSeleccionada.mostrarPorcentajeLlenado=this.porcentajeLlenadoCheckbox;
    //this.seleccionAenviar.emit(this.seleccion);
    //this.infoPorcentajeLlenado.emit(this.porcentajeLlenadoCheckbox);
  }

  nivelLlenadoClick() {
    this.datosSeleccionados.filtroNivelLlenado = this.nivelLlenado;
    this.objetoDatosSeleccionados.emit(this.datosSeleccionados);
    //this.seleccion.filtrosSeleccionados.nivelLlenado=this.nivelLlenado;
    //this.seleccionAenviar.emit(this.seleccion);
  }

  selectAllAlertas() {
    for (var i = 0; i < this.alertas.length; i++) {
      this.alertas[i].selected = this.selectedAllAlertas;
    }
    if (this.selectedAllAlertas) {
      //por si ya estaban seleccionadas todas al momento de seleccionar la opción selectAll
      //if (this.datosSeleccionados.filtroAlertas.length != this.alertas.length) {
      for (let alerta of this.alertas) {
        if (!this.datosSeleccionados.filtroAlertas.includes(alerta.name)) {
          this.datosSeleccionados.filtroAlertas.push(alerta.name);
        }
      }
      //this.objetoDatosSeleccionados.emit(this.datosSeleccionados);
      //}
    } else {
      this.datosSeleccionados.filtroAlertas = [];
      //this.objetoDatosSeleccionados.emit(this.datosSeleccionados);
    }
    this.objetoDatosSeleccionados.emit(this.datosSeleccionados);
  }

  checkIfAllSelectedAlertas(alertaSeleccionada: any) {
    this.datosSeleccionados;
    this.selectedAllAlertas = this.alertas.every(function (item: any) {
      //if (item.selected) {
      //instanceDatosSeleccionados.filtroAlertas.push(item2);
      //this.datosSeleccionados.filtroAlertas.push(item);
      //} else {
      //const index: number = instanceDatosSeleccionados.filtroAlertas.indexOf(item2);
      //if (index !== -1) {
      //  instanceDatosSeleccionados.filtroAlertas.splice(index, 1);
      // }
      //}
      return item.selected == true;
    })
    if (alertaSeleccionada.selected) {
      this.datosSeleccionados.filtroAlertas.push(alertaSeleccionada.name);
    } else {
      const index: number = this.datosSeleccionados.filtroAlertas.indexOf(alertaSeleccionada.name);
      if (index !== -1) {
        this.datosSeleccionados.filtroAlertas.splice(index, 1);
      }
    }
    this.objetoDatosSeleccionados.emit(this.datosSeleccionados);
  }

  /*
  todasAlertasChequeadas() {
   
    this.checkTodasAlertas = !this.checkTodasAlertas;
    this.datosSeleccionados.filtroAlertas = [];
    this.datosSeleccionados.filtroAlertas.push('todas');
    this.objetoDatosSeleccionados.emit(this.datosSeleccionados);
  }
*/
  /*
    alertaIncendioChecked() {
     
      if (!this.checkTodasAlertas) {
        if (this.alertaIncendioChecked) {
          this.datosSeleccionados.filtroAlertas.push('incendio');
          this.objetoDatosSeleccionados.emit(this.datosSeleccionados);
        } else {
          const index: number = this.datosSeleccionados.filtroAlertas.indexOf('incendio');
          if (index !== -1) {
            this.datosSeleccionados.filtroAlertas.splice(index, 1);
            this.objetoDatosSeleccionados.emit(this.datosSeleccionados);
          }
        }
  
      }
    }
    */

  openDialog(texto: string, tipo: string) {
    const modalRef = this.modalService.open(DialogComponent, { centered: true });
    modalRef.componentInstance.mensaje = texto;
    modalRef.componentInstance.tipoDialogo = tipo;
    modalRef.result.then(
      (closeResult) => {
        //modal close  
        console.log("modal closed : ", closeResult);
      }, (dismissReason) => {
        //modal Dismiss  
        if (dismissReason == ModalDismissReasons.ESC) {
          console.log("modal dismissed when used pressed ESC button");
        } else if (dismissReason == ModalDismissReasons.BACKDROP_CLICK) {
          console.log("modal dismissed when used pressed backdrop");
        } else {
          console.log(dismissReason);
        }
      })
  }
}
