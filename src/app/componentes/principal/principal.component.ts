import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { SeleccionPanelTiempoReal } from '../../interfaces/seleccion-panel-tiempo-real.interface';
import { ObjetoSeleccionTiempoReal } from '../../interfaces/objetoDataSeleccionadaTiempoReal.interface';
import { ContenidoReporte } from '../../interfaces/respuestaReporte.interface';
import { ApiService } from "../../servicios/api-service";
import { DialogComponent } from '../../helpers/dialog/dialog.component';
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css'],
})
export class PrincipalComponent implements OnInit {
  //tipoReporte = "";
  reporte: ContenidoReporte;
  visualizacionSeleccionada = "";
  variable: any;

  circuitos: any;
  municipios: any;// = [{ "MunicipioId": 1, "Codigo": "A", "Limites": [] }, { "MunicipioId": 2, "Codigo": "B", "Limites": [[-34.92929, -56.1611671], [-34.88903, -56.16563], [-34.88776, -56.1646843], [-34.8895226, -56.18211], [-34.8904381, -56.18168], [-34.8914948, -56.1862259], [-34.8904381, -56.18597], [-34.8899422, -56.1879425], [-34.89093, -56.1882858], [-34.8912125, -56.2031364], [-34.89635, -56.2003021], [-34.9058533, -56.2233047], [-34.91275, -56.2169533], [-34.9114838, -56.20262], [-34.9170456, -56.1854553], [-34.91613, -56.1717224], [-34.91937, -56.1737], [-34.9239426, -56.173008], [-34.92929, -56.1611671]] }, { "MunicipioId": 3, "Codigo": "C", "Limites": [[-34.8790321, -56.1555862], [-34.8766365, -56.15095], [-34.8460732, -56.19009], [-34.87213, -56.2266541], [-34.8829727, -56.1998749], [-34.8914223, -56.2031364], [-34.8908577, -56.1882], [-34.8897324, -56.18786], [-34.8902969, -56.18597], [-34.8914223, -56.186142], [-34.8901558, -56.1820221], [-34.88959, -56.18185], [-34.88762, -56.1646843], [-34.8852272, -56.16314], [-34.8790321, -56.1555862]] }, { "MunicipioId": 4, "Codigo": "CH", "Limites": [[-34.9373131, -56.16022], [-34.88889, -56.1655426], [-34.88579, -56.1634827], [-34.8790321, -56.1554146], [-34.888607, -56.1473465], [-34.886776, -56.14237], [-34.8863525, -56.1377335], [-34.89959, -56.1222839], [-34.9040947, -56.1222839], [-34.91226, -56.131897], [-34.9111328, -56.14065], [-34.9132423, -56.1452866], [-34.9185944, -56.1473465], [-34.919014, -56.14374], [-34.9284439, -56.1578178], [-34.9373131, -56.16022]] }, { "MunicipioId": 5, "Codigo": "D", "Limites": [] }, { "MunicipioId": 6, "Codigo": "E", "Limites": [] }, { "MunicipioId": 7, "Codigo": "F", "Limites": [] }, { "MunicipioId": 8, "Codigo": "G", "Limites": [] }];
  //circuitosAux: any = [{ "MunicipioId": 4, "Circuitos": [{ "CircuitoId": 1, "Codigo": "CH_1", "Municipio": "CH", "Limites": [[-34.9152832, -56.14876], [-34.91166, -56.1455421], [-34.9075775, -56.14949], [-34.90677, -56.1495361], [-34.9068031, -56.1504364], [-34.9087753, -56.1548576], [-34.90916, -56.1534424], [-34.9106064, -56.152195], [-34.91215, -56.15357], [-34.91409, -56.1534424], [-34.9152832, -56.14876]] }, { "CircuitoId": 2, "Codigo": "CH_2", "Municipio": "CH", "Limites": [[-34.91409, -56.1534424], [-34.9192619, -56.15619], [-34.92183, -56.15065], [-34.92081, -56.1493645], [-34.91838, -56.14855], [-34.9152145, -56.14889], [-34.91409, -56.1534424]] }, { "CircuitoId": 3, "Codigo": "CH_3", "Municipio": "CH", "Limites": [[-34.91099, -56.16314], [-34.9119759, -56.16022], [-34.9087029, -56.15481], [-34.9091263, -56.1534424], [-34.91064, -56.1522827], [-34.91219, -56.153656], [-34.91409, -56.1534843], [-34.91891, -56.15606], [-34.9165878, -56.1584625], [-34.91838, -56.16121], [-34.9171867, -56.1624527], [-34.91099, -56.16314]] }, { "CircuitoId": 4, "Codigo": "CH_4", "Municipio": "CH", "Limites": [[-34.91722, -56.1624527], [-34.9183464, -56.16121], [-34.9165535, -56.1584625], [-34.9189453, -56.15606], [-34.9192619, -56.15619], [-34.92183, -56.1506081], [-34.92929, -56.16112], [-34.91722, -56.1624527]] }] }, { "MunicipioId": 3, "Circuitos": [{ "CircuitoId": 6, "Codigo": "C_1", "Municipio": "C", "Limites": [[-34.87086, -56.1823235], [-34.86287, -56.17919], [-34.863678, -56.1768723], [-34.86579, -56.17348], [-34.862236, -56.1700478], [-34.86794, -56.1625824], [-34.8698769, -56.1677742], [-34.87086, -56.1823235]] }, { "CircuitoId": 7, "Codigo": "C_2", "Municipio": "C", "Limites": [[-34.88505, -56.19614], [-34.88498, -56.1946831], [-34.88403, -56.19481], [-34.8839226, -56.19378], [-34.88283, -56.19138], [-34.8837128, -56.1907768], [-34.8820572, -56.1873], [-34.883255, -56.1867], [-34.8810539, -56.1863136], [-34.8827972, -56.1817551], [-34.88809, -56.1847458], [-34.889267, -56.1816254], [-34.8906479, -56.1813774], [-34.8918343, -56.1863365], [-34.8926544, -56.1866455], [-34.8919334, -56.18874], [-34.8910446, -56.18831], [-34.8915977, -56.1954956], [-34.88505, -56.19614]] }] }, { "MunicipioId": 2, "Circuitos": [{ "CircuitoId": 5, "Codigo": "B_1", "Municipio": "B", "Limites": [[-34.895752, -56.18168], [-34.8923721, -56.1753273], [-34.89167, -56.1652], [-34.88769, -56.1656723], [-34.88924, -56.18168], [-34.8906136, -56.1815071], [-34.8912125, -56.1842537], [-34.895752, -56.18168]] }] }];
  municipioSeleccionado: any;
  circuitoSeleccionado: any;

  reporteTieneVisualizacionConMapa:boolean=true;

  itemsSeleccionadosTiempoReal: SeleccionPanelTiempoReal;
  /*itemsSeleccionadosTiempoReal = {
    informacion:{ 
      mostrarAlertas: false,
      mostrarPorcentajeLlenado:false
    },
    filtros: {
      nivelLlenado : '',
      alertas:[]
    }
  };
  */
  datosSeleccionadosTiempoReal: ObjetoSeleccionTiempoReal;
  conVisualizacionMapa= false;

  constructor(public cd: ChangeDetectorRef, public apiService: ApiService, private modalService: NgbModal) {

    //this.cd.detach();
  }

  ngOnInit() {
    this.visualizacionSeleccionada = 'TiempoReal';
    this.municipioSeleccionado = "";
    this.circuitoSeleccionado = "";
    //this.municipios=['1','2'];
    var url = "api/adminContenedor/municipios";
    this.apiService.get(url).subscribe(
      resp => {
        var ok = resp["Exito"];
        var msj = resp["Mensaje"];
        if (!ok) {
          this.openDialog(msj, "Error")
        } else {
          var datos = resp["Datos"];
          if (datos == null || datos.length == 0) {
            this.openDialog(msj, "Atención");
          } else {
            this.municipios = datos;
          }
        }
      },
      err => {
        console.log(err);
        console.log("Error occured.")
      }
    )

  }

  municipioSeleccionadoChange() {
    this.circuitoSeleccionado = "";
    if (this.municipioSeleccionado == "") {
      this.circuitoSeleccionado = "";
      this.circuitos = [];
    } else {
      var municipioId = this.municipioSeleccionado.MunicipioId;
      var url = "api/adminContenedor/circuitosMunicipioId?municipioId=" + municipioId;
      this.apiService.get(url).subscribe(
        resp => {
          var ok = resp["Exito"];
          var msj = resp["Mensaje"];
          if (!ok) {
            this.openDialog(msj, "Error")
          } else {
            var datos = resp["Datos"];
            if (datos == null || datos.length == 0) {
              //this.openDialog(msj, "Atención");
            } else {
              this.circuitos = datos;
              console.log(resp);
            }
          }
        },
        err => {
          console.log(err);
          console.log("Error occured.")
        }
      )

      /*this.circuitosAux.forEach(municipio => {
        
        if (municipio.MunicipioId == municipioId) {
          this.circuitos = municipio.Circuitos;
        }
      });*/
    }
  }

  setearDatosRecibidosPorHijoTiempoReal(seleccion: SeleccionPanelTiempoReal) {
    this.itemsSeleccionadosTiempoReal = seleccion;
    this.cd.detectChanges();

  }

  setearEnMapaPorcLlenado(checked: boolean) {
    this.variable = checked;

  }

  setearDatosRecibidosPorTiempoReal(datos: ObjetoSeleccionTiempoReal) {
    //this.datosSeleccionadosTiempoReal=datos;
    this.datosSeleccionadosTiempoReal =
      {
        filtroNivelLlenado: datos.filtroNivelLlenado,
        filtroAlertas: datos.filtroAlertas,
        mostrarAlertas: datos.mostrarAlertas,
        mostrarPorcentajeLlenado: datos.mostrarPorcentajeLlenado
      }
  }

  /*setearReporte(reporte: string) {
    
    this.tipoReporte = reporte;
  }*/
 /* setearTipoReporte(esConMapa: boolean) {
    
    this.conVisualizacionMapa = esConMapa;
  }
*/
  enviarRespReporte(reporteRecibido: ContenidoReporte) {
    
    this.reporte = {
      nombreReporte: reporteRecibido.nombreReporte,
      itemsReporte: reporteRecibido.itemsReporte
    };
  }

  openDialog(texto: string, tipo: string) {
    const modalRef = this.modalService.open(DialogComponent, { centered: true });
    modalRef.componentInstance.mensaje = texto;
    modalRef.componentInstance.tipoDialogo = tipo;
    modalRef.result.then(
      (closeResult) => {
        //modal close  
        console.log("modal closed : ", closeResult);
      }, (dismissReason) => {
        //modal Dismiss  
        if (dismissReason == ModalDismissReasons.ESC) {
          console.log("modal dismissed when used pressed ESC button");
        } else if (dismissReason == ModalDismissReasons.BACKDROP_CLICK) {
          console.log("modal dismissed when used pressed backdrop");
        } else {
          console.log(dismissReason);
        }
      })
  }

  setearReporteSeleccionadoVisualizacionMapa(visualizacionMapa){
    debugger;
    this.conVisualizacionMapa=visualizacionMapa;
  }


}
