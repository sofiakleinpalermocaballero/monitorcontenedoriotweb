import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { CustomValidators } from "../../../helpers/customValidators";
import { AuthService } from '../../../servicios/auth.service'
import { ApiService } from "../../../servicios/api-service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { EdicionPasswordComponent } from '../../gestionUsuarios/edicion-password/edicion-password.component'

@Component({
  selector: 'app-alta-edicion-usuario',
  templateUrl: './alta-edicion-usuario.component.html',
  styleUrls: ['./alta-edicion-usuario.component.css']
})
export class AltaEdicionUsuarioComponent implements OnInit {

  @Input() seEstaModificandoPropioUsuario: boolean = false;

  @Input() alta: boolean = false;

  @Input() usuario: any = {
    usuarioId: 0,
    nombre: "",
    apellido: "",
    correo: "",
    contraseña1: "",
    contraseña2: "",
    rol: ""
  }

  usuarioDto = {
    UsuarioId: null
    , Nombre: null
    , Apellido: null
    , Email: null
    , RolId: null
  }
  error: string = "";
  mensaje: string = "";
  titulo: string = "";
  erroresParaDesplegar: any = null;
  mensajeErrorVerificacionMail: any = null;
  roles: any[];
  form: FormGroup;

  objetoBindeado = {
    apiSer: null,
    user: null
  }

  constructor(private modalService: NgbModal,public authService: AuthService, public apiService: ApiService, public activeModal: NgbActiveModal) {

    console.log(this.usuario);
    this.obtenerRoles();
  }



  ngOnInit() {

    if (!this.alta) {//es una edicion
      this.usuario.contraseña1 = "";
      this.usuario.contraseña2 = "";
      this.titulo = "Editar usuario"
    } else {
      this.usuario.usuarioId = 0;
      this.titulo = "Crear usuario"
    }
    this.objetoBindeado.apiSer = this.apiService;
    this.objetoBindeado.user = this.usuario
    this.crearFormulario();
  }

  crearFormulario() {
    this.form = new FormGroup({
      //1er parametro:valor por defecto
      //2do:reglas de validacion
      //3er: reglas de validación asíncronas
      'nombre': new FormControl(this.usuario.nombre, CustomValidators.required),
      'apellido': new FormControl(this.usuario.apellido, CustomValidators.required),
      'correo': new FormControl({ value: this.usuario.correo, disabled: !this.alta }, [CustomValidators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")]),
      'contraseña1': new FormControl({ value: '', disabled: !this.alta }, [CustomValidators.required, Validators.minLength(6)]),
      'contraseña2': new FormControl({ value: '', disabled: !this.alta }),
      'rol': new FormControl({ value: this.usuario.rol, disabled: this.seEstaModificandoPropioUsuario }, Validators.required)
    });
    this.form.controls['contraseña2'].setValidators([
      CustomValidators.required,
      this.contraseñasIguales.bind(this.form)
    ])
    //this.form.controls['correo'].setAsyncValidators([
    //  this.emailEnUso.bind(this.objetoBindeado)
    //]);

    /*this.form.controls['contraseña1'].setValidators([
    CustomValidators.required,
    this.contraseñasIguales.bind(this.form),
    Validators.minLength(6)
    ])*/

    //this.form.setValue(this.usuario);
  }

  obtenerRoles() {
    var url = 'api/manejadorUsuarios/roles';
    this.apiService.get(url).subscribe(
      resp => {
        var ok = resp["Exito"];
        var msj = resp["Mensaje"];
        if (!ok) {
          this.erroresParaDesplegar = msj;
        } else {
          var datos = resp["Datos"];
          this.roles = datos;
        }
      },
      err => {
        console.log(err);
        console.log("Error occured.")
      })
  }

  guardarUsuario() {
    this.error = "";
    this.mensaje = "";
    debugger;
    console.log("Estado del formulario:", this.form)
    if (this.form.invalid) {
      return;
    }
    let valoresFormulario = this.form.value;
    var nombreFormulario = valoresFormulario.nombre;
    var apellidoFormulario = valoresFormulario.apellido;
    var rolFormulario = valoresFormulario.rol;
    if (this.alta) {
      var emailFormulario = valoresFormulario.correo;
      var contraseñaFormulario = valoresFormulario.contraseña1;
      if (this.authService.altaUsuarioCredenciales(emailFormulario, contraseñaFormulario)) {
        debugger;
        this.usuarioDto.Nombre = nombreFormulario;
        this.usuarioDto.Apellido = apellidoFormulario;
        this.usuarioDto.Email = emailFormulario;
        this.usuarioDto.RolId = rolFormulario;
        this.darDeAltaUsuario();

      }
    } else {
      debugger;
      this.usuarioDto.Nombre = nombreFormulario;
      this.usuarioDto.Apellido = apellidoFormulario;
      this.usuarioDto.UsuarioId = this.usuario.usuarioId;
      if (!this.seEstaModificandoPropioUsuario) {
        this.usuarioDto.RolId = rolFormulario
      } else {
        this.usuarioDto.RolId = this.usuario.rol;
      }
      this.editarUsuario();
    }
  }

  darDeAltaUsuario() {
    var url = 'api/manejadorUsuarios/crearUsuario';
    this.apiService.post(url, this.usuarioDto).subscribe(
      resp => {
        debugger;
        var ok = resp["Exito"];
        var msj = resp["Mensaje"];
        if (!ok) {
          this.error = msj;
        } else {
          this.mensaje = "Usuario creado exitosamente";
          this.form.reset({
            nombre: "",
            apellido: "",
            correo: "",
            rol: ""
          });
        }
      },
      err => {
        console.log(err);
        console.log("Error occured.")
      })
  }

  editarUsuario() {
    var url = "api/manejadorUsuarios/actualizarUsuario";
    this.apiService.post(url, this.usuarioDto).subscribe(
      resp => {
        debugger;
        var ok = resp["Exito"];
        var msj = resp["Mensaje"];
        if (!ok) {
          this.error = msj;
        } else {
          //localStorage.setItem('usuarioLogueadoNombreApellido', this.usuarioDto.Nombre +" "+ this.usuarioDto.Apellido);
          this.mensaje = "Usuario editado exitosamente";
        }
      },
      err => {
        console.log(err);
        console.log("Error occured.")
      }
    )
  }

  emailEnUso(control: FormControl): Promise<any> {

    let apiService: any = this.objetoBindeado.apiSer;//apiSer;
    let usuario = this.objetoBindeado.user;//user;

    //let apiService: any = this[0];
    //let usuario: any = this[1];
    console.log("sdjnsjealwfefqfkas", usuario.usuarioId);
    //let mensajeErrorVerificacionMail :any = this.mensajeErrorVerificacionMail;
    let promesa = new Promise(
      (resolve, reject) => {
        setTimeout(() => {
          var correoIngresado = control.value;
          var url = 'api/manejadorUsuarios/emailEnUso?email=' + correoIngresado;
          apiService.get(url).subscribe(
            resp => {
              var ok = resp["Exito"];
              var msj = resp["Mensaje"];
              if (!ok) {
                resolve({ errorVerificacionMail: true });
                //mensajeErrorVerificacionMail = msj;
              } else {
                var respuesta = resp["Datos"];
                if (respuesta) {
                  resolve({ mailEnUso: true });
                } else {
                  resolve(null);
                }
              }
            },
            err => {
              console.log(err);
              console.log("Error occured.")
            })
        }, 3000)
      }
    )
    return promesa;
  }

  /*this.erroresParaDesplegar=null;
     var url = '"api/manejadorUsuarios/emailEnUso?email='+"";
     this.apiService.get(url).subscribe(
       resp => {
         var ok = resp["Exito"];
         var msj = resp["Mensaje"];
         if (!ok) {
           this.erroresParaDesplegar="Error: "+ msj;
         } else {
           var datos = resp["Datos"];
           if (datos == null || datos.length == 0) {
             this.openDialog(msj, "Atención");
           } else {
             this.municipios = datos;
           }
         }
       },
       err => {
         console.log(err);
         console.log("Error occured.")
       }*/


  contraseñasIguales(control: FormControl): any {
    let form: any = this;
    if (control.value !== form.controls['contraseña1'].value) {
      return {
        passDiferentes: true
      }
    }
    return null;

  }

  /*contraseñasIguales1(control: FormControl) : any {
    let form:any=this;
    
    var valorContraseña2=form.controls['contraseña2'].value;
    if ( valorContraseña2!="" && control.value !== form.controls['contraseña2'].value) {
      return {
        passDiferentes:true
      }
    }
    return null; 
    
  }*/


  cambiarPassword() {
    // this.authService.resetarContraseña(this.usuario.correo);
    //this.authService.resetarContraseña("sofiaklein@hotmail.com");
    debugger;
    const modalRef = this.modalService.open(EdicionPasswordComponent, { centered: true });
   // modalRef.componentInstance.emailUsuario = this.usuario.correo;

    //modalRef.componentInstance.alta = true;

    modalRef.result.then(
      (closeResult) => {
        debugger;
        console.log("modal closed : ", closeResult);
      }, (dismissReason) => {
        debugger;
        //modal Dismiss  
        if (dismissReason == ModalDismissReasons.ESC) {
          console.log("modal dismissed when used pressed ESC button");
        } else if (dismissReason == ModalDismissReasons.BACKDROP_CLICK) {
          console.log("modal dismissed when used pressed backdrop");
        } else {
          console.log(dismissReason);
        }
      })
  }

  clickFormsInput() {
    debugger;
    this.error = "";
    this.mensaje = "";
  }

}
