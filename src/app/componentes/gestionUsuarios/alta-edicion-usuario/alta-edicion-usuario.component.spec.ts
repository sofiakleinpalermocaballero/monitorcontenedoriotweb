import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaEdicionUsuarioComponent } from './alta-edicion-usuario.component';

describe('AltaEdicionUsuarioComponent', () => {
  let component: AltaEdicionUsuarioComponent;
  let fixture: ComponentFixture<AltaEdicionUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaEdicionUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaEdicionUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
