import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { AltaEdicionUsuarioComponent } from '../../gestionUsuarios/alta-edicion-usuario/alta-edicion-usuario.component'
import { ApiService } from "../../../servicios/api-service";

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})

export class UsuariosComponent implements OnInit {
  headElements = ['ID', 'First', 'Last', 'Handle'];
  
  errores: string = "";
  usuarios : any[];
  usuarioSeleccionado: any;
  

  constructor(private modalService: NgbModal, public apiService: ApiService) {
    this.obtenerUsuarios();
  }

  ngOnInit() {
  }

  obtenerUsuarios() {
    
    var url = 'api/manejadorUsuarios/obtenerUsuarios';
    this.apiService.get(url).subscribe(
      resp => {
        
        var ok = resp["Exito"];
        var msj = resp["Mensaje"];
        if (!ok) {
          this.errores = msj;
        } else {
          var datos = resp["Datos"];
          this.usuarios = datos;
        }
      },
      err => {
        this.errores = err;
        console.log(err);
        console.log("Error occured.")
      })
  }

  onSelectProduct(user: any) {
    this.usuarioSeleccionado;
  }

  crearUsuario() {
    const modalRef = this.modalService.open(AltaEdicionUsuarioComponent, { centered: true });

    modalRef.componentInstance.alta = true;

    modalRef.result.then(
      (closeResult) => {
        debugger;
        console.log("modal closed : ", closeResult);
        this.obtenerUsuarios();
      }, (dismissReason) => {
        debugger;
        //modal Dismiss  
        if (dismissReason == ModalDismissReasons.ESC) {
          console.log("modal dismissed when used pressed ESC button");
        } else if (dismissReason == ModalDismissReasons.BACKDROP_CLICK) {
          console.log("modal dismissed when used pressed backdrop");
        } else {
          console.log(dismissReason);
        }
        debugger;
        this.obtenerUsuarios();
      })
  }

  editarUsuario(user:any) {
    
    const modalRef = this.modalService.open(AltaEdicionUsuarioComponent, { centered: true });

    modalRef.componentInstance.alta = false;
    debugger;
    if(localStorage.getItem('usuarioLogueadoId')==user.UsuarioId){
      modalRef.componentInstance.seEstaModificandoPropioUsuario=true;
    }else{
      modalRef.componentInstance.seEstaModificandoPropioUsuario=false;
    }
    modalRef.componentInstance.usuario = {
      usuarioId:user.UsuarioId,
      nombre: user.Nombre,
      apellido: user.Apellido,
      correo: user.Email,
      rol:user.RolUsuario.RolId
    };

    modalRef.result.then(
      (closeResult) => {
        console.log("modal closed : ", closeResult);
        this.obtenerUsuarios();
      }, (dismissReason) => {
        //modal Dismiss  
        if (dismissReason == ModalDismissReasons.ESC) {
          console.log("modal dismissed when used pressed ESC button");
        } else if (dismissReason == ModalDismissReasons.BACKDROP_CLICK) {
          console.log("modal dismissed when used pressed backdrop");
        } else {
          console.log(dismissReason);
        }
        this.obtenerUsuarios();
      })
  }

}
