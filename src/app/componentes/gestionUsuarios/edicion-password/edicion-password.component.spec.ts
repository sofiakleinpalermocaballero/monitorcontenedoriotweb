import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdicionPasswordComponent } from './edicion-password.component';

describe('EdicionPasswordComponent', () => {
  let component: EdicionPasswordComponent;
  let fixture: ComponentFixture<EdicionPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdicionPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdicionPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
