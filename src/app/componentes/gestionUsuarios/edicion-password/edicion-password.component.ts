import { Component, OnInit,Input} from '@angular/core';
import { AuthService } from '../../../servicios/auth.service'
import * as firebase from 'firebase/app';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { CustomValidators } from "../../../helpers/customValidators";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-edicion-password',
  templateUrl: './edicion-password.component.html',
  styleUrls: ['./edicion-password.component.css']
})
export class EdicionPasswordComponent implements OnInit {
  form: FormGroup;
  error: string = "";
  mensaje: string = "";

  //@Input() emailUsuario: boolean = false;

  constructor(public authService: AuthService, public activeModal: NgbActiveModal) {
    this.crearFormulario();
  }

  ngOnInit() {
    debugger;

  }
  crearFormulario() {
    this.form = new FormGroup({
      'contraseñaVieja': new FormControl('', ),
      'contraseña1': new FormControl('',),
      'contraseña2': new FormControl(''),
    });
    this.form.controls['contraseña2'].setValidators([
      CustomValidators.required,
      this.contraseñasIguales.bind(this.form)
    ])
  }

  contraseñasIguales(control: FormControl): any {
    let form: any = this;
    if (control.value !== form.controls['contraseña1'].value) {
      return {
        passDiferentes: true
      }
    }
    return null;
  }

  guardarNuevaContrasena() {
    this.error = "";
    this.mensaje = "";
    debugger;
    if (this.form.invalid) {
      return;
    }
    let valoresFormulario = this.form.value;
    var contraseñaVieja = valoresFormulario.contraseñaVieja;
    var contraseñaNueva = valoresFormulario.contraseña2;

    const cpUser = this.authService._firebaseAuth.auth.currentUser;

    const credentials = firebase.auth.EmailAuthProvider.credential(
      cpUser.email, contraseñaVieja);

    //Reauthenticating here with the data above
    cpUser.reauthenticateWithCredential(credentials).then(res => {
      debugger;
      /* Update the password to the password the user typed into the
               new password input field */
      cpUser.updatePassword(contraseñaNueva).then(res => {
        //Success
        debugger;
        this.mensaje = "Operación exitosa";
        this.form.reset({
          contraseña1: "",
          contraseña2: "",
          contraseñaVieja: "",
        });
      })
        .catch(error => {
          debugger;
          if(error.code=="auth/weak-password"){
            this.error="Contraseña no suficientemente segura."
          }else{
            this.error = "No fue posible cambiar la contraseña. Intente nuevamente más tarde.";
          }
          
          //Failed
        });
    })
      .catch(error => {
        console.log(error);
        debugger;
        if (error.code === "auth/wrong-password") {
          this.error = "Contraseña anterior inválida"
        }
      });
  }

  clickFormsInput(){
    this.error="";
    this.mensaje="";
  }

}
