import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import * as L from 'leaflet'

@Component({
  selector: 'app-mapa-flotante-alerta',
  templateUrl: './mapa-flotante-alerta.component.html',
  styleUrls: ['./mapa-flotante-alerta.component.css']
})
export class MapaFlotanteAlertaComponent implements OnInit {
  mapa: any;
  @Input() alerta: any;
  alertIcon: any;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  
    //if (this.mapa == undefined) {
      this.initializeMap();
    //}
  }

  /*
  closeModal() {
    this.activeModal.close('Modal Closed');
  }
  */

  mostrarAlerta() {
  
    var coordinates = L.latLng([this.alerta.Ubicacion.latitude, this.alerta.Ubicacion.longitude]);
    L.marker(coordinates, {
      icon: this.alertIcon
    }).addTo(this.mapa);
    this.mapa.setView(coordinates, 16);
  }

  initializeMap() {
   
    // this.mimapa = new L.Map('mapaFlotante');
    this.mapa = L.map('mapaFlotante'/*,
      {
        zoomControl: false
      }*/);
    this.alertIcon = L.icon({
      iconUrl: 'assets/img/alerta.svg',
      iconSize: [48, 55],
      iconAnchor: [24, 55],
      popupAnchor: [0, -55]
    });

    L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png', {
      attribution: '<a href="https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">Wikimedia</a>'
    }).addTo(this.mapa);


    this.mostrarAlerta();
  }


}
