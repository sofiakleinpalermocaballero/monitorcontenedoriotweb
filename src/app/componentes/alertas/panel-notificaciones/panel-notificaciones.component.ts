import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';


@Component({
  selector: 'app-panel-notificaciones',
  templateUrl: './panel-notificaciones.component.html',
  styleUrls: ['./panel-notificaciones.component.css']
})
export class PanelNotificacionesComponent implements OnInit {

  //listaAlertas:any;
  listaAlertas: Observable<any[]>;

  constructor(afs: AngularFirestore) { 
    this.listaAlertas = afs.collection('alertas').valueChanges();
    //afs.doc("29").delete();
  }

  ngOnInit() {
/*
    this.listaAlertas= [
      {
          titulo: "SERES VIVOS",
          emision: "12/11/2018 13:56",
          municipio: "CH",
          circuito: "b12",
      },
      {
          titulo: "VOLCADO",
          emision: "12/11/2018 13:56",
          municipio: "B",
          circuito: "C12",
      },
      {
        titulo: "FUEGO",
        emision: "17/11/2015 13:56",
        municipio: "C",
        circuito: "12",
    }
    ];
    */  
  }




  

}
