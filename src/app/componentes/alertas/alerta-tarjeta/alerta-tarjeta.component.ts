import { Component, OnInit, Input } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
//import { NgbModal } from "bootstrap/js/dist/modal";
import { MapaFlotanteAlertaComponent } from "../mapa-flotante-alerta/mapa-flotante-alerta.component";
//declare let LMap;
import { ApiService } from "../../../servicios/api-service";
import {
  AngularFirestore,
  AngularFirestoreDocument,
  AngularFirestoreCollection
} from "@angular/fire/firestore";

@Component({
  selector: "app-alerta-tarjeta",
  templateUrl: "./alerta-tarjeta.component.html",
  styleUrls: ["./alerta-tarjeta.component.css"]
})
export class AlertaTarjetaComponent implements OnInit {
  @Input() alerta: any = {};

  closeResult: string;
  isSelected: boolean = false;
  errores: string = "";

  constructor(private modalService: NgbModal, public apiService: ApiService) {}

  ngOnInit() {
    debugger;
  }

  desplegarAlertaEnMapa() {
    this.isSelected = true;
    const modalRef = this.modalService.open(MapaFlotanteAlertaComponent, {
      centered: true
    });
    // {centered: true,backdrop: false,keyboard: false }/* { backdrop: false, keyboard: false , ariaLabelledBy: 'modal-basic-title' }*/);
    //size: 'lg'
    modalRef.componentInstance.alerta = this.alerta;

    modalRef.result.then(
      closeResult => {
        //modal close
        this.isSelected = false;
        console.log("modal closed : ", closeResult);
      },
      dismissReason => {
        this.isSelected = false;

        //modal Dismiss
        if (dismissReason == ModalDismissReasons.ESC) {
          console.log("modal dismissed when used pressed ESC button");
        } else if (dismissReason == ModalDismissReasons.BACKDROP_CLICK) {
          console.log("modal dismissed when used pressed backdrop");
        } else {
          console.log(dismissReason);
        }
      }
    );
    /* 
     modalRef.result.then((result) => {
       console.log(result);
     }).catch((error) => {
       console.log(error);
     });
     
     */
    /*   modalRef.result.then(
         (result) => {
           this.closeResult = `Closed with: ${result}`;
         }, (reason) => {
           this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
         });
         */
  }

  desactivarAlerta() {
    debugger;
    var url = 'api/manejadorAlertas/desactivarAlerta';
    this.apiService.get(url).subscribe(
      resp => {
        debugger;
        var ok = resp["Exito"];
        var msj = resp["Mensaje"];
        if (!ok) {
          this.errores = msj;
        } else {
          //var datos = resp["Datos"];
         
        }
      },
      err => {
        this.errores = err;
        console.log(err);
        console.log("Error occured.")
      })
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
