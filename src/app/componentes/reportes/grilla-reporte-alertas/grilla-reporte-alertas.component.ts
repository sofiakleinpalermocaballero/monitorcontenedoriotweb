import { Component, OnInit ,Input} from '@angular/core';
import { ContenidoReporte } from '../../../interfaces/respuestaReporte.interface';
import { NgxDatatableModule } from "@swimlane/ngx-datatable";

@Component({
  selector: 'app-grilla-reporte-alertas',
  templateUrl: './grilla-reporte-alertas.component.html',
  styleUrls: ['./grilla-reporte-alertas.component.css']
})
export class GrillaReporteAlertasComponent implements OnInit {
  @Input() contenidoReporte: ContenidoReporte;

  constructor() { 
    
  }

  ngOnInit() {
    
  }

}
