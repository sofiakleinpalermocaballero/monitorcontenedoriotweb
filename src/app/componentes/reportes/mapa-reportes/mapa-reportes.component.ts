import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ContenidoReporte } from '../../../interfaces/respuestaReporte.interface';
import * as L from 'leaflet';

@Component({
  selector: 'app-mapa-reportes',
  templateUrl: './mapa-reportes.component.html',
  styleUrls: ['./mapa-reportes.component.css']
})
export class MapaReportesComponent implements OnInit {
  //usados por leaflet:
  mymap: any;
  //icons:
  greenIcon: any;
  yellowIcon: any;
  redIcon: any;
  alertIcon: any;
  groupMarkers: any;

  //@Input() tipoDeReporte : string;
  @Input() contenidoReporte: ContenidoReporte;

  constructor() { }

  ngOnInit() {
    this.initializeMap();
  }

  ngOnChanges(cambio: SimpleChanges) {
    
    if (!cambio.contenidoReporte.firstChange) {
      this.desplegarContenidoReporte();
    }

  }

  initializeMap() {
    this.mymap = L.map('mapReportes');

    //L.tileLayer('https://korona.geog.uni-heidelberg.de/tiles/roads/x={x}&y={y}&z={z}', {
    //	attribution: 'Imagery from <a href="http://giscience.uni-hd.de/">GIScience Research Group @ University of Heidelberg</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    //}).addTo(mymap);

    L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png', {
      attribution: '<a href="https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">Wikimedia</a>'
    }).addTo(this.mymap);

    this.greenIcon = L.icon({
      iconUrl: 'assets/img/icon_verde.svg',
      iconSize: [48, 55],
      iconAnchor: [24, 55],
      popupAnchor: [0, -55]
    });

    this.yellowIcon = L.icon({
      iconUrl: 'assets/img/icon_amarillo.svg',
      iconSize: [48, 55],
      iconAnchor: [24, 55],
      popupAnchor: [0, -55]
    });

    this.redIcon = L.icon({
      iconUrl: 'assets/img/icon_rojo.svg',
      iconSize: [48, 55],
      iconAnchor: [24, 55],
      popupAnchor: [0, -55]
    });

    this.alertIcon = L.icon({
      iconUrl: 'assets/img/alerta.svg',
      iconSize: [38, 45],
      iconAnchor: [20, 95],
      popupAnchor: [0, -55]
    });

    this.centrarMapa();
  }

  centrarMapa() {
    this.mymap.setView([-34.89437925718939, -56.16554260253906], 13);

  }

  desplegarContenidoReporte() {
    
    var markers = [];

    this.contenidoReporte.itemsReporte.forEach((item: any) => {

      //var mostrarContenedor = true;
      //console.log(container.nivel_llenado);
      //var geoLocation = item.ubicacion;

      var coordinates = L.latLng(item.Coordenadas);

      var icon: any;
      icon = this.greenIcon;
      if (this.contenidoReporte.nombreReporte == "llenadoPromedio") {
        //icon = this.greenIcon;

        if (item.NivelPromedio == 'medio') {
          icon = this.yellowIcon;
        }
        if (item.NivelPromedio == 'alto') {
          icon = this.redIcon;
        }
      }
      if (this.contenidoReporte.nombreReporte == "contenedoresVaciados") {
        //icon = this.greenIcon;
        if (!item.Vaciado) {
          icon = this.redIcon;
        }
      }
     /* var contenido = "Municipio: " + item.Municipio + "<br />" +
        "Circuito: " + item.Circuito + "<br />";
      if(this.contenidoReporte.nombreReporte=="reporteAlertas"){
        contenido = contenido + "Alerta de incendio <br />";
      }
      */
     

      var mun_circ_pos = item.Circuito + "_" + item.Posicion;

      if (this.groupMarkers != undefined) {
        this.mymap.removeLayer(this.groupMarkers);
      }


      var marker = L.marker(coordinates/*.value.coordinates*/, {
        icon: icon,
        //title: mun_circ_pos
      });
      if (this.contenidoReporte.nombreReporte == "llenadoPromedio") {
        marker.bindTooltip(item.PorcentajeLlenadoPromedio + "%",
          {
            permanent: true,
            direction: 'bottom'
          }).openTooltip();
      }
      if(this.contenidoReporte.nombreReporte=="frecuenciaPromedio"){
        //icon = this.greenIcon;
        marker.bindTooltip(item.TiempoPromedio,
        {
          permanent: true,
          direction: 'bottom'
        }).openTooltip();
      }
      marker.bindPopup(mun_circ_pos);
      markers.push(marker);

    });

    this.groupMarkers = L.featureGroup(markers).addTo(this.mymap);
    this.mymap.fitBounds(this.groupMarkers.getBounds(), { padding: [0, 0] });

  }
}
