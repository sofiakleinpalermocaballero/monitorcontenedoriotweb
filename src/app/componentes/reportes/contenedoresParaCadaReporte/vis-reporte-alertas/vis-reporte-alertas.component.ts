import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { ApiService } from "../../../../servicios/api-service";
import { DialogComponent } from '../../../../helpers/dialog/dialog.component';
import { ContenidoReporte } from '../../../../interfaces/respuestaReporte.interface';
import { DatePipe } from '@angular/common';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-vis-reporte-alertas',
  templateUrl: './vis-reporte-alertas.component.html',
  styleUrls: ['./vis-reporte-alertas.component.css']
})
export class VisReporteAlertasComponent implements OnInit {
  selectedAllAlertas: any;
  
  errorFechaInicioNull = false;
  errorFechaFinNull = false;
  fechasSolapadas = false;
  horaInicioSeleccionada = "23:46";
  horaFinSeleccionada= "23:46";

  fechaInicial: any;
  fechaFinal: any;

  @Input() filtroMunicipio: any;
  @Input() filtroCircuito: any;

  @Output() respuestaReporteEmitida: EventEmitter<ContenidoReporte>;


  respReporte: ContenidoReporte = {
    nombreReporte: '',
    itemsReporte: []
  };

  parametrosReporte = {
    MunicipioId: null
    , CircuitoId: null
    , FechaInicio: null
    , FechaFin: null
    , Estado: null
    , IncluirTiempoDesdeRecAnterior: null
    , TipoAlertaId: []
    , NombreReporte: null
  }

  alertas: any;
  checkBoxNoActivas:boolean=false;
  checkBoxActivas:boolean=false;

  constructor(public apiService: ApiService, private modalService: NgbModal) { 
    this.alertas = [];
    this.obtenerAlertas();
  }

  ngOnInit() {
  }

  obtenerAlertas() {
    
    var url = "api/manejadorAlertas/tiposAlertas";
    this.apiService.get(url).subscribe(
      resp => {
        
        var ok = resp["Exito"];
        var msj = resp["Mensaje"];
        if (!ok) {
          this.openDialog(msj, "Error")
        } else {
          var datos = resp["Datos"];
          if (datos == null || datos.length == 0) {
            this.openDialog(msj, "Atención");
          } else {
            


            /*      { name: 'Incendio', selected: false },
                  { name: 'Seres vivos', selected: false },
                  { name: 'Funcionamiento', selected: false },
                  { name: 'Volcado', selected: false } */

            datos.forEach((alerta: any) => {
              
              var item = { name: alerta.Descripcion, id:alerta.TipoAlertaId ,selected: false };
              this.alertas.push(item);
            })
          }
        }

      },
      err => {
        console.log(err);
        console.log("Error occured.")
      }
    )
  }

  validacionesYseteo() {
    this.errorFechaInicioNull = false;
    this.errorFechaFinNull = false;
    this.fechasSolapadas = false;
    if (this.fechaInicial == undefined || this.fechaInicial == "" || this.fechaFinal == undefined || this.fechaFinal == "") {
      if (this.fechaInicial == undefined || this.fechaInicial == "") {
        this.errorFechaInicioNull = true;
      }
      if (this.fechaFinal == undefined || this.fechaFinal == "") {
        this.errorFechaFinNull = true;
      }
      return false;
    }
    var fechaInicioNro = +this.fechaInicial.replace(/-/g, "");
    var fechaFinNro = +this.fechaFinal.replace(/-/g, "");
    if (fechaInicioNro > fechaFinNro) {
      this.fechasSolapadas = true;
      return false;
    }
    this.parametrosReporte.MunicipioId
    if (this.filtroMunicipio != "") {
      this.parametrosReporte.MunicipioId = this.filtroMunicipio.MunicipioId;
    }else{
      this.parametrosReporte.MunicipioId=null;
    }
    if (this.filtroCircuito != "") {
      this.parametrosReporte.CircuitoId = this.filtroCircuito.CircuitoId;
    }
    else{
      this.parametrosReporte.CircuitoId=null;
    }
    var datePipe = new DatePipe("en-US");

    var fechaInicio = new Date(datePipe.transform(this.fechaInicial, 'MM/dd/yyyy'));
    var fechaFin = new Date(datePipe.transform(this.fechaFinal, 'MM/dd/yyyy'));
    if (this.horaInicioSeleccionada != "" && this.horaInicioSeleccionada != undefined) {
      let horaInicioSplit = this.horaInicioSeleccionada.split(":")
      fechaInicio.setHours(+horaInicioSplit[0], +horaInicioSplit[1]);
    }
    if (this.horaFinSeleccionada != "" && this.horaFinSeleccionada != undefined) {
      let horaFinSplit = this.horaFinSeleccionada.split(":")
      fechaFin.setHours(+horaFinSplit[0], +horaFinSplit[1]);
    }
    this.parametrosReporte.FechaInicio = fechaInicio;
    this.parametrosReporte.FechaFin = fechaFin;

    this.parametrosReporte.TipoAlertaId=[];
    if(!this.selectedAllAlertas){
      this.alertas.forEach((alerta: any) => {
        if(alerta.selected){
          this.parametrosReporte.TipoAlertaId.push(alerta.id);
        }
        
      })
    }
    if (this.checkBoxActivas && this.checkBoxNoActivas) {
      this.parametrosReporte.Estado = "Todos";
    } else {
      if (this.checkBoxNoActivas) {
        this.parametrosReporte.Estado = "Inactivas";
      } else {
        if(this.checkBoxActivas){
          this.parametrosReporte.Estado = "Activas";
        }else{
          this.parametrosReporte.Estado = "Todos";
        }       
      }
    }
    this.parametrosReporte.NombreReporte = "HistoricoAlertas";
    return true;
  }

  obtenerReporte() {

    if (this.validacionesYseteo()) {


      /*let httpHeaders = new HttpHeaders({
        'Content-Type' : 'application/json',
        'Cache-Control': 'no-cache'
      });
      let options = {
        headers: httpHeaders
      }; 
     
      var jsonObject = JSON.stringify(this.parametrosReporte); 
       */
      var url = 'api/reportes/obtenerReporte';
      this.apiService.post(url, this.parametrosReporte).subscribe(
        resp => {
          
          var ok = resp["Exito"];
          var msj = resp["Mensaje"];
          if (!ok) {
            this.openDialog(msj, "Error")
          } else {
            var datos = resp["Datos"];
            if (datos == null || datos.length == 0) {
              this.openDialog(msj, "Atención");
            } else {
              
              this.respReporte.nombreReporte = "reporteAlertas";
              this.respReporte.itemsReporte = datos;
              this.respuestaReporteEmitida.emit(this.respReporte);
            }
          }

        },
        err => {
          console.log(err);
          console.log("Error occured.")
        }
      )
    }
  }

  openDialog(texto: string, tipo: string) {
    const modalRef = this.modalService.open(DialogComponent, { centered: true });
    modalRef.componentInstance.mensaje = texto;
    modalRef.componentInstance.tipoDialogo = tipo;
    modalRef.result.then(
      (closeResult) => {
        //modal close  
        console.log("modal closed : ", closeResult);
      }, (dismissReason) => {
        //modal Dismiss  
        if (dismissReason == ModalDismissReasons.ESC) {
          console.log("modal dismissed when used pressed ESC button");
        } else if (dismissReason == ModalDismissReasons.BACKDROP_CLICK) {
          console.log("modal dismissed when used pressed backdrop");
        } else {
          console.log(dismissReason);
        }
      })
  }
  
  exportar() {
    if (this.validacionesYseteo()) {

      var url = 'api/reportes/exportarReporte';
      this.apiService.post(url, this.parametrosReporte).subscribe(
        resp => {
          
          var ok = resp["Exito"];
          var msj = resp["Mensaje"];
          if (!ok) {
            this.openDialog(msj, "Error")
          } else {
            var datos = resp["Datos"];
            if (datos == null || datos.length == 0) {
              this.openDialog(msj, "Atención");
            } else {
              var file = new Blob([this.s2ab(atob(datos))], { type: "application/vnd.ms-excel" });
              saveAs(file, 'HistoricoAlertas.xlsx');
            }
          }

        },
        err => {
          console.log(err);
          console.log("Error occured.")
        }
      )
    }

  }

  s2ab(s: string) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i)
      view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

  selectAllAlertas() {
    for (var i = 0; i < this.alertas.length; i++) {
      this.alertas[i].selected = this.selectedAllAlertas;
    }
  }
  checkIfAllSelectedAlertas(alertaSeleccionada: any) {
    this.selectedAllAlertas = this.alertas.every(function (item: any) {
      return item.selected == true;
    })
  }
}
