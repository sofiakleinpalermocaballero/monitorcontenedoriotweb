import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ApiService } from "../../../../servicios/api-service";
import { ContenidoReporte } from '../../../../interfaces/respuestaReporte.interface';
//import { MatDialog, MatDialogConfig } from "@angular/material";
import { DialogComponent } from '../../../../helpers/dialog/dialog.component';
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-vis-reporte-vaciados-turno',
  templateUrl: './vis-reporte-vaciados-turno.component.html',
  styleUrls: ['./vis-reporte-vaciados-turno.component.css']
})
export class VisReporteVaciadosTurnoComponent implements OnInit {
  @Input() filtroMunicipio: any;
  @Input() filtroCircuito: any;

  turnoSeleccionado: any;
  checkBoxVaciado: boolean = true;
  checkBoxNoVaciado: boolean = false;
  fecha: any;
  errorFechaNull: any;
  errorTurnoNull: any;
  turnos: any;

  parametrosReporte = {
    MunicipioId: null
    , CircuitoId: null
    , Fecha: null
    , TurnoId: null
    , Estado: null
    , NombreReporte:null
  }

  @Output() respuestaReporteEmitida: EventEmitter<ContenidoReporte>;
  respReporte: ContenidoReporte = {
    nombreReporte: '',
    itemsReporte: []
  };

  constructor(public apiService: ApiService, private modalService: NgbModal) {
    
    this.respuestaReporteEmitida = new EventEmitter();
  }

  ngOnInit() {
    
    this.turnoSeleccionado = "";
    this.obtenerTurnos();
  }

  obtenerTurnos() {
    
    var url = "api/adminContenedor/turnosRecoleccion"
    this.apiService.get(url).subscribe(
      resp => {
        
        this.turnos = resp["Datos"];
        console.log(resp);
      },
      err => {
        console.log(err);
        console.log("Error occured.")
      }
    )
  }

  validacionesYseteos() {
    var error = false;
    this.errorFechaNull = false;
    this.errorTurnoNull = false;
    if (!this.checkBoxVaciado && !this.checkBoxNoVaciado) {
      error = true;
    }
    if (this.fecha == undefined || this.fecha == "") {
      this.errorFechaNull = true;
      error = true;
    }

    if (this.turnoSeleccionado == "") {
      this.errorTurnoNull = true;
      error = true;
    }
    if (error) {
      return false;
    }

    var datePipe = new DatePipe("en-US");
    var fechaReporte = new Date(datePipe.transform(this.fecha, 'MM/dd/yyyy'));

    if (this.filtroMunicipio != "") {
      this.parametrosReporte.MunicipioId = this.filtroMunicipio.MunicipioId;
    }else{
      this.parametrosReporte.MunicipioId=null;
    }
    if (this.filtroCircuito != "") {
      this.parametrosReporte.CircuitoId = this.filtroCircuito.CircuitoId;
    }else{
      this.parametrosReporte.CircuitoId=null;
    }

    this.parametrosReporte.Fecha = fechaReporte;
    this.parametrosReporte.TurnoId = this.turnoSeleccionado;
    if (this.checkBoxVaciado && this.checkBoxNoVaciado) {
      this.parametrosReporte.Estado = "Todos";
    } else {
      if (this.checkBoxNoVaciado) {
        this.parametrosReporte.Estado = "NoVaciados";
      } else {
        this.parametrosReporte.Estado = "Vaciados";
      }
    }
    this.parametrosReporte.NombreReporte="ContenedoresVaciados";
    return true;
  }
  obtenerReporte() {
    
    if (this.validacionesYseteos()) {
      var url = 'api/reportes/obtenerReporte';
      this.apiService.post(url, this.parametrosReporte).subscribe(
        resp => {
          //if(resp.Estado)
          
          var ok = resp["Exito"];
          var msj = resp["Mensaje"];
          if (!ok) {
            this.openDialog(msj, "Error")
          } else {
            var datos = resp["Datos"];
            if (datos == null || datos.length == 0) {
              this.openDialog(msj, "Atención");
            } else {
              this.respReporte.nombreReporte = "contenedoresVaciados";
              this.respReporte.itemsReporte = datos;
              this.respuestaReporteEmitida.emit(this.respReporte);
            }
          }
        },
        err => {
          console.log(err);
          console.log("Error occured.")
        }
      )
    }
  }

  openDialog(texto: string, tipo: string) {
    const modalRef = this.modalService.open(DialogComponent, { centered: true });
    modalRef.componentInstance.mensaje = texto;
    modalRef.componentInstance.tipoDialogo = tipo;
    modalRef.result.then(
      (closeResult) => {
        //modal close  
        console.log("modal closed : ", closeResult);
      }, (dismissReason) => {
        //modal Dismiss  
        if (dismissReason == ModalDismissReasons.ESC) {
          console.log("modal dismissed when used pressed ESC button");
        } else if (dismissReason == ModalDismissReasons.BACKDROP_CLICK) {
          console.log("modal dismissed when used pressed backdrop");
        } else {
          console.log(dismissReason);
        }
      })
  }

  exportar() {
    
    if (this.validacionesYseteos()) {
      var url = 'api/reportes/exportarReporte';
      this.apiService.post(url, this.parametrosReporte).subscribe(
        resp => {
          
          var ok = resp["Exito"];
          var msj = resp["Mensaje"];
          if (!ok) {
            this.openDialog(msj, "Error")
          } else {
            var datos = resp["Datos"];
            if (datos == null || datos.length == 0) {
              this.openDialog(msj, "Atención");
            } else {
              var file = new Blob([this.s2ab(atob(datos))], { type: "application/vnd.ms-excel" });
              saveAs(file, 'ContenedoresVaciadosEnUnTurno.xlsx');
            }
          }

        },
        err => {
          console.log(err);
          console.log("Error occured.")
        }
      )
    }
  }

  s2ab(s: string) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i)
      view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

}
