import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisReporteFuncionamientoComponent } from './vis-reporte-funcionamiento.component';

describe('VisReporteFuncionamientoComponent', () => {
  let component: VisReporteFuncionamientoComponent;
  let fixture: ComponentFixture<VisReporteFuncionamientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisReporteFuncionamientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisReporteFuncionamientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
