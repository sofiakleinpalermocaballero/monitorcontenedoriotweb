import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
//import { HttpClient } from '@angular/common/http';
//import { HttpHeaders } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { ContenidoReporte } from '../../../../interfaces/respuestaReporte.interface';
//import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { ApiService } from "../../../../servicios/api-service";
import { DialogComponent } from '../../../../helpers/dialog/dialog.component';
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-vis-reporte-historico-recolecciones',
  templateUrl: './vis-reporte-historico-recolecciones.component.html',
  styleUrls: ['./vis-reporte-historico-recolecciones.component.css']
})
export class VisReporteHistoricoRecoleccionesComponent implements OnInit {

  errorFechaInicioNull = false;
  errorFechaFinNull = false;
  fechasSolapadas = false;

  @Input() filtroMunicipio: any;
  @Input() filtroCircuito: any;
  @Output() respuestaReporteEmitida: EventEmitter<ContenidoReporte>;
  respReporte: ContenidoReporte = {
    nombreReporte: '',
    itemsReporte: []
  };

  fechaInicial: any;
  fechaFinal: any;

  respuesta: any;

  parametrosReporte = {
    MunicipioId: null
    , CircuitoId: null
    , FechaInicio: null
    , FechaFin: null
    ,NombreReporte: null
  }

  constructor(public apiService: ApiService, private modalService: NgbModal) {
    this.respuestaReporteEmitida = new EventEmitter();
  }

  ngOnInit() {
    /*this.formElement = this.fb.group({
      FechaComienzo: [Validators.required],
    });
    setTimeout(() => this.showForm = true);
    */
  }

  ngOnChanges(cambio: SimpleChanges) {  
    console.log("cambios:");
    console.log(cambio);
  }

  validacionesYseteo() {
    this.errorFechaInicioNull = false;
    this.errorFechaFinNull = false;
    this.fechasSolapadas = false;
    if (this.fechaInicial == undefined || this.fechaInicial == "" || this.fechaFinal == undefined || this.fechaFinal == "") {
      if (this.fechaInicial == undefined || this.fechaInicial == "") {
        this.errorFechaInicioNull = true;
      }
      if (this.fechaFinal == undefined || this.fechaFinal == "") {
        this.errorFechaFinNull = true;
      }
      return false;
    }
    var fechaInicioNro = +this.fechaInicial.replace(/-/g, "");
    var fechaFinNro = +this.fechaFinal.replace(/-/g, "");
    if (fechaInicioNro > fechaFinNro) {
      this.fechasSolapadas = true;
      return false;
    }
    this.parametrosReporte.MunicipioId
    if (this.filtroMunicipio != "") {
      this.parametrosReporte.MunicipioId = this.filtroMunicipio.MunicipioId;
    }else{
      this.parametrosReporte.MunicipioId=null;
    }
    if (this.filtroCircuito != "") {
      this.parametrosReporte.CircuitoId = this.filtroCircuito.CircuitoId;
    }else{
      this.parametrosReporte.CircuitoId=null;
    }
    var datePipe = new DatePipe("en-US");
    var fechaInicio = new Date(datePipe.transform(this.fechaInicial, 'MM/dd/yyyy'));
    var fechaFin = new Date(datePipe.transform(this.fechaFinal, 'MM/dd/yyyy'));
    this.parametrosReporte.FechaInicio = fechaInicio;
    this.parametrosReporte.FechaFin = fechaFin;
    this.parametrosReporte.NombreReporte = "HistoricoRecoleccion";
    return true;

  }

  obtenerReporte() {
    if (this.validacionesYseteo()) {
      var url = 'api/reportes/obtenerReporte';
      this.apiService.post(url, this.parametrosReporte).subscribe(
        resp => {
          var ok = resp["Exito"];
          var msj = resp["Mensaje"];
          if (!ok) {
            this.openDialog(msj, "Error")
          } else {
            var datos = resp["Datos"];
            if (datos == null || datos.length == 0) {
              this.openDialog(msj, "Atención");
            } else {
              
              this.respReporte.nombreReporte = "llenadoPromedio";
              this.respReporte.itemsReporte = datos;
              this.respuestaReporteEmitida.emit(this.respReporte);
            }
          }

        },
        err => {
          console.log(err);
          console.log("Error occured.")
        }
      )
    }
  }

  openDialog(texto: string, tipo: string) {
    const modalRef = this.modalService.open(DialogComponent, { centered: true });
    modalRef.componentInstance.mensaje = texto;
    modalRef.componentInstance.tipoDialogo = tipo;
    modalRef.result.then(
      (closeResult) => {
        //modal close  
        console.log("modal closed : ", closeResult);
      }, (dismissReason) => {
        //modal Dismiss  
        if (dismissReason == ModalDismissReasons.ESC) {
          console.log("modal dismissed when used pressed ESC button");
        } else if (dismissReason == ModalDismissReasons.BACKDROP_CLICK) {
          console.log("modal dismissed when used pressed backdrop");
        } else {
          console.log(dismissReason);
        }
      })
  }


  exportar() {
    if (this.validacionesYseteo()) {
      var url = 'api/reportes/exportarReporte';
      this.apiService.post(url, this.parametrosReporte).subscribe(
        resp => {
          
          var ok = resp["Exito"];
          var msj = resp["Mensaje"];
          if (!ok) {
            this.openDialog(msj, "Error")
          } else {
            var datos = resp["Datos"];
            if (datos == null || datos.length == 0) {
              this.openDialog(msj, "Atención");
            } else {
              var file = new Blob([this.s2ab(atob(datos))], { type: "application/vnd.ms-excel" });
              saveAs(file, 'ReporteLLenadoPromedioRecoleccion.xlsx'); 
            }
          }

        },
        err => {
          console.log(err);
          console.log("Error occured.")
        }
      )
    }
    
  }

  s2ab(s: string) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i)
      view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }
  

}
