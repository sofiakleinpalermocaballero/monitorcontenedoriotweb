import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ContenidoReporte } from '../../../interfaces/respuestaReporte.interface';

@Component({
  selector: 'app-visualizacion-reportes',
  templateUrl: './visualizacion-reportes.component.html',
  styleUrls: ['./visualizacion-reportes.component.css']
})
export class VisualizacionReportesComponent implements OnInit {
  reporteSeleccionado = ""

  @Input() filtroMunicipio: any;
  @Input() filtroCircuito: any;
  //@Output() tipoReporteSeleccionado: EventEmitter<string>;
  @Output() respuestaReporteEmitida: EventEmitter<ContenidoReporte>;
  @Output() tieneVisualizacionMapa: EventEmitter<boolean>;

  constructor() {
    
    this.respuestaReporteEmitida = new EventEmitter();
    this.tieneVisualizacionMapa = new EventEmitter();
    //this.tipoReporteSeleccionado = new EventEmitter();
  }

  ngOnInit() {
    
    this.reporteSeleccionado = 'LlenadoRecolecc';
    this.tieneVisualizacionMapa.emit(true);
    //this.tipoReporteSeleccionado.emit(this.reporteSeleccionado);
  }

  cambioReporte(){
    debugger;
    if(this.reporteSeleccionado=="ContVaciadosEnTurno" || this.reporteSeleccionado=="TiempoRecolecc" || this.reporteSeleccionado=="LlenadoRecolecc" ){
      this.tieneVisualizacionMapa.emit(true);
    }else{
      this.tieneVisualizacionMapa.emit(false);
    }
    //this.tipoReporteSeleccionado.emit(this.reporteSeleccionado);
  }

  ngOnChanges(cambio: SimpleChanges) {
  
    console.log("cambios:");
    console.log(cambio);
  }

  enviarRespReporte(infoReporte: ContenidoReporte){
    
    this.respuestaReporteEmitida.emit(infoReporte);
  }
}
