import { Directive, EventEmitter, ElementRef, HostListener, Input, Output } from '@angular/core';
import { FileItem } from '../models/file-item';
import { DialogComponent } from '../helpers/dialog/dialog.component';
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";

@Directive({
  selector: '[appNgDropFiles]'
})
export class NgDropFilesDirective {

  @Input() archivos: FileItem[] = [];
  @Output() mouseSobre: EventEmitter<boolean> = new EventEmitter();

  constructor(private modalService: NgbModal) { }

  // Eventos////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  @HostListener('dragover', ['$event'])
  public onDragEnter( event: any ) {
    this.mouseSobre.emit( true );
    this._prevenirDetener( event );
  }

  @HostListener('dragleave', ['$event'])
  public onDragLeave( event: any ) {
    this.mouseSobre.emit( false );
  }

  @HostListener('drop', ['$event'])
  public onDrop( event: any ) {
    debugger;
    const transferencia = this._getTransferencia( event );

    if ( !transferencia ) {
      return;
    }

    this._extraerArchivos( transferencia.files );
    this._prevenirDetener( event );
    this.mouseSobre.emit( false );

  }

  // Acciones////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  private _getTransferencia( event: any ) {
    return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer;
  }

  private _extraerArchivos( archivosLista: FileList ) {

    // console.log( archivosLista );

    // tslint:disable-next-line:forin
    /*if(archivosLista.length>1){
      this.openDialog("Solo se puede cargar un archivo a la vez", "Atención");
      alert("nwfkefnwek")
      return;
    }*/
    for ( const propiedad in Object.getOwnPropertyNames( archivosLista ) ) {

      const archivoTemporal = archivosLista[propiedad];

      if ( this._archivoPuedeSerCargado( archivoTemporal ) ) {

        const nuevoArchivo = new FileItem( archivoTemporal );
        this.archivos.push( nuevoArchivo );

      }
    }
    console.log(this.archivos);
  }


  // Validaciones////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  private _archivoPuedeSerCargado( archivo: File ): boolean {

    if ( !this._archivoYaFueDroppeado( archivo.name ) && this._esJson( archivo.type, archivo.name )) {
      return true;
    } else {
      return false;
    }

  }


  private _prevenirDetener( event ) {
    event.preventDefault();
    event.stopPropagation();
  }

  private _archivoYaFueDroppeado( nombreArchivo: string ): boolean {

    for ( const archivo of this.archivos ) {

      if ( archivo.nombreArchivo === nombreArchivo  ) {
        console.log('El archivo ' + nombreArchivo + ' ya esta agregado');
        return true;
      }

    }

    return false;
  }

  private _esJson( tipoArchivo:string, nombreArchivo: string ): boolean {
    debugger;
    if(tipoArchivo==='application/json'){
      return true;
    }else{
      var terminacionGeoJson = nombreArchivo.slice(-8);
      var extensionGeoJson = terminacionGeoJson.slice(-7);
      return terminacionGeoJson[0]==='.' && extensionGeoJson==='geojson'
    }
    /*var extensionJson = nombreArchivo.slice(-4);
    var extensionGeoJson = nombreArchivo.slice(-7);
    return extensionJson==='json' || extensionGeoJson==='geojson';*/
   // return ( tipoArchivo === '' || tipoArchivo === undefined ) ? false : tipoArchivo.startsWith('image');
  }
  /*
  private _esImagen( tipoArchivo: string ): boolean {
    return ( tipoArchivo === '' || tipoArchivo === undefined ) ? false : tipoArchivo.startsWith('image');
  }
*/
  openDialog(texto: string, tipo: string) {
    const modalRef = this.modalService.open(DialogComponent, { centered: true });
    modalRef.componentInstance.mensaje = texto;
    modalRef.componentInstance.tipoDialogo = tipo;
    modalRef.result.then(
      (closeResult) => {
        //modal close  
        console.log("modal closed : ", closeResult);
      }, (dismissReason) => {
        //modal Dismiss  
        if (dismissReason == ModalDismissReasons.ESC) {
          console.log("modal dismissed when used pressed ESC button");
        } else if (dismissReason == ModalDismissReasons.BACKDROP_CLICK) {
          console.log("modal dismissed when used pressed backdrop");
        } else {
          console.log(dismissReason);
        }
      })
  }

}
