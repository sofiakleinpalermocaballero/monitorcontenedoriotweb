import { Injectable, NgZone } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class ApiService {
    baseUri: string = environment.baseUri;

    constructor(private http: HttpClient) {
       
    }

    get(url: string): Observable<any> {
        return this.http.get(this.baseUri + url);
    }

    post(url: string, obj: any): Observable<any> {
        return this.http.post(this.baseUri+url,obj);
    }

}