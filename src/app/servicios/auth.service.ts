import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
//import { auth } from 'firebase/app';
import { AngularFirestore } from '../../../node_modules/@angular/fire/firestore';
//import { Observable } from 'rxjs';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import * as firebase from 'firebase/app';
import { ApiService } from "../servicios/api-service";
import { HttpClient } from '@angular/common/http';
//import {BehaviorSubject} from 'rxjs';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  //authState: FirebaseAuthState = null;
  private user: Observable<firebase.User>;
  public usuarioLogueado: any = {};
  private userDetails: firebase.User = null;
  public userUid: any;
  rolActual: any;
  prueba: boolean;
  baseUri: string = environment.baseUri;

  //isInitDone = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, private afs: AngularFirestore, public _firebaseAuth: AngularFireAuth, private router: Router, public apiService: ApiService) {
    console.log("instance AuthService created");
    this.prueba = false;
    /*firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        var displayName = user.displayName;
        var email = user.email;
        var emailVerified = user.emailVerified;
        var photoURL = user.photoURL;
        var isAnonymous = user.isAnonymous;
        var uid = user.uid;
        var providerData = user.providerData;
        // ...
      } else {
        // User is signed out.
        // ...
      }
    });
*/



    /* this._firebaseAuth.authState.subscribe(user => {
 
       console.log('Estado del usuario', user);
       if (!user) {
         return;
       }
     })
 */

    this._firebaseAuth.authState.subscribe(
      (user) => {
        console.log("Estado del usuario: ", user);

        /*if (user) {
          this.userDetails = user;
          console.log(this.userDetails);
          this.userUid=user.uid;
        } else {
          this.userDetails = null;
          this.userUid=null;
        }*/
        if (!user) {
          return;
        }
        this.userUid = user.uid;
      }
    );
  }

  /*
  login() {
    this.afAuth.auth.signInWithPopup(new auth.EmailAuthProvider());
  }
  logout() {
    this.afAuth.auth.signOut();
  }
  */
  /*
    public getHighScores(): Observable<Array<HighScore>> {
      return this.http
          .get(this.baseUrl)
          .map((response: Response) => {
              return <Array<HighScore>> response.json();
          });
    }
  */

  async tieneAccesoAfuncionalidad() {
    debugger;
    var respuesta = await this.rolUsuario();
    debugger;
    var ok = respuesta["Exito"];
    var msj = respuesta["Mensaje"];
    if (!ok) {
      console.log(msj);
      return false;
    } else {
      var datosUsuario = respuesta["Datos"];
      localStorage.setItem('usuarioLogueadoNombreApellido', datosUsuario.Nombre +" "+ datosUsuario.Apellido);
      localStorage.setItem('usuarioLogueadoId', datosUsuario.UsuarioId);
      var rol = datosUsuario.RolUsuario.Descripcion;
      return rol != "Consultor";
    }



    // this.isInitDone.subscribe(isAvailable => {

    //   if (isAvailable) {
    debugger;
    //  var mail = localStorage.getItem('mailUsuarioLogueado');
    //  var url = "api/manejadorUsuarios/datosUsuario?email=" + mail;
    /*  this.http
      .get(this.baseUri + url)
      .map((response: Response) => {
        return true;
    });*/
    /*  this.apiService.get(url).subscribe(
        resp => {
          debugger;
          var ok = resp["Exito"];
          var msj = resp["Mensaje"];
          if (!ok) {
            console.log(msj);
            return of(false);
            

          } else {
            var datosUsuario = resp["Datos"];
            localStorage.setItem('usuarioLogueadoNombreApellido', datosUsuario.Nombre + datosUsuario.Apellido);
            this.rolActual = datosUsuario.RolUsuario.Descripcion;
            return of(this.rolActual!="Consultor");
          }
        },
        err => {
          console.log(err);
          console.log("Error occured.")
          return of(false);
        });*/
    //return of(false);
    //    }
    // });

  }

  rolUsuario() {
    var mail = localStorage.getItem('mailUsuarioLogueado');
    return this.http.get('http://localhost:49570/api/manejadorUsuarios/datosUsuario?email=' + mail).toPromise();
  }

  altaUsuarioCredenciales(email: string, password: string) {

    return this._firebaseAuth.auth.createUserWithEmailAndPassword(email, password)
      .then(value => {

        console.log('Success!', value);
        return true;
      })
      .catch(err => {

        console.log('Something went wrong:', err.message);
        return false;
      });
  }

  isLoggedIn() {

    //
    /*if (this.userUid == null) {
      return false;
    } else {
      return true;
    }*/
    /* if (this._firebaseAuth.auth.currentUser == null) {
       return false;
     }
     return true;
     */
    return this.prueba;
  }

  inicioSesionRegular(mail: string, pass: string) {
    //debugger;
    this.prueba = true;
    //const credential = firebase.auth.EmailAuthProvider.credential(mail, pass);
    return this._firebaseAuth.auth.signInWithEmailAndPassword(mail, pass)
      .then(res => {
        //debugger;
        return res;
        //this.isInitDone.next(true);

        console.log(res)

        console.log(res)

        //return true;
      })
      .catch(error => {
        //debugger;
        throw error;
        console.log(error)
        //return false;
      });

  }

  logout() {
    this.userUid = null;
    this.prueba = false;
    this._firebaseAuth.auth.signOut()
      .then((res) => this.router.navigateByUrl('/login'));
  }

  resetarContraseña(email: string) {
    return this._firebaseAuth.auth.sendPasswordResetEmail(email)
      .then(value => {
        console.log('Success!', value);
        return true;
      })
      .catch(err => {
        console.log('Something went wrong:', err.message);
        return false;
      });
  }
}
