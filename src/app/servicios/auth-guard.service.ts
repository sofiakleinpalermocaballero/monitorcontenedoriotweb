import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import {
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate
} from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})

export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private authService: AuthService) { }

  async canActivate() {
    debugger;
    var resp = await this.authService.tieneAccesoAfuncionalidad();
    if (resp) {
      debugger;
      return true;
    } else {
      debugger;
      //this.router.navigate(['/']);
      this.router.navigate(['inicio'])
      return false;
    }
debugger;
    // if (this.authService.tieneAccesoAfuncionalidad()) {
    //   return true;
    // }

    // return this.authService.tieneAccesoAfuncionalidad();
  }
}

@Injectable()
export class AuthGuardLogin implements CanActivate {

  constructor(private router: Router, private authService: AuthService) {
  }

  canActivate() {

    if (!this.authService.isLoggedIn()) {
      return true;
    }
    this.router.navigate(['inicio']);
    return false;
  }
}

@Injectable()
export class AuthGuardLogueado implements CanActivate {

  constructor(private router: Router, private authService: AuthService) {
  }

  canActivate() {

    if (this.authService.isLoggedIn()) {
      //this.router.navigate(['login']);
      return true;
    }
    this.router.navigate(['/']);
    return false;
  }
}
