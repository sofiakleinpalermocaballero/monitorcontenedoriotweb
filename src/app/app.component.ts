import { Component, } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'MonitorContenedorIotWeb';
  visible:boolean = false;
  constructor(public router:Router) { 
    this.router.events.subscribe(event => {
      if(event.constructor.name === "NavigationEnd") {
        this.visible = this.router.url !== '/login';
      }
    });
  }
}
