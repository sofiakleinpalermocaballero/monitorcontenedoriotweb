import {RouterModule, Routes} from '@angular/router';
import {AuthGuardService, AuthGuardLogin, AuthGuardLogueado} from './servicios/auth-guard.service';
import {PrincipalComponent} from './componentes/principal/principal.component';
import {ConfiguracionesComponent} from './componentes/configuraciones/configuraciones.component';
import {InicioComponent} from './componentes/inicio/inicio.component';
import {LoginComponent} from './componentes/login/login.component';
import {UsuariosComponent} from './componentes/gestionUsuarios/usuarios/usuarios.component';
import {ImportacionContenedoresComponent} from './componentes/contenedores/importacion-contenedores/importacion-contenedores.component';
import {CircuitosComponent} from './componentes/circuitos/circuitos.component'

const APP_ROUTES: Routes = [
    {path: 'login', component: LoginComponent, canActivate: [AuthGuardLogin]},
    {path: 'inicio', component: InicioComponent, canActivate: [AuthGuardLogueado]},
    {path: 'usuarios', component: UsuariosComponent, canActivate: [AuthGuardService]},
    {path: 'circuitos', component: CircuitosComponent , canActivate: [AuthGuardService]},
    {path: 'contenedores', component: ImportacionContenedoresComponent , canActivate: [AuthGuardService]},
    {path: 'configuraciones', component: ConfiguracionesComponent , canActivate: [AuthGuardService]},
    {path: '**', pathMatch: 'full', redirectTo: 'login'},
    /*{ path: '',   redirectTo: 'login', pathMatch: 'full' },*/
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
